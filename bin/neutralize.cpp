#include <cmath>
#include <cstdint>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <memory>

#include <glm/vec3.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

#include <webp/decode.h>

namespace fs = std::filesystem;

static double invert_value(double x, const double v)
{
	if(v == 0)
	{
		return x;
	}
	return x * (1.0 - v) / v;
}

static double max3(double a, double b, double c)
{
	return std::max(std::max(a, b), c);
}

static uint_fast32_t limit;
static bool neg = false;
static uint_fast32_t max_i = 0;

static glm::dvec3 neutralize(const glm::dvec3& rgb)
{
	double r = rgb.x;
	double g = rgb.y;
	double b = rgb.z;

	uint_fast32_t i = 0;
	while(true)
	{
		if(i >= limit)
		{
			//std::cout << "loop stopped; initial value = " << glm::to_string(rgb) << "; current value = " << glm::to_string(glm::dvec3(r, g, b)) << "\n";
			break;
		}
		if(neg)
		{
			r = 1.0 - r;
			g = 1.0 - g;
			b = 1.0 - b;
		}

		double v = max3(r, g, b);
		double r2, g2, b2;
		if(neg)
		{
			r2 = invert_value(r, v);
			g2 = invert_value(g, v);
			b2 = invert_value(b, v);
		}
		else
		{
			r2 = 1.0 - invert_value(r, v);
			g2 = 1.0 - invert_value(g, v);
			b2 = 1.0 - invert_value(b, v);
		}
		if(r2 == r && g2 == g && b2 == b)
		{
			break;
		}
		r = r2;
		g = g2;
		b = b2;
		++i;
	}
	if(i > max_i)
	{
		max_i = i;
	}

	return glm::dvec3(r, g, b);
}

static uint_fast8_t d_to_p(double d)
{
	bool neg = false;
	if(d < 0)
	{
		d = -d;
		neg = true;
	}
	d = std::max(std::min(d, 255.0), 0.0);
	uint_fast8_t n = std::round(d);
	if(neg)
	{
		return 255 - n;
	}
	return n;
}

static double darken_amount = 0;

static glm::dvec3 pixel_to_vec(const glm::ivec3& pixel)
{
	return (glm::dvec3(pixel) + darken_amount) / (255.0 + darken_amount);
}

static glm::ivec3 vec_to_pixel(const glm::dvec3& vec)
{
	#define t(x) d_to_p(x * (255.0 + darken_amount) - darken_amount)
	return
	{
		t(vec.x),
		t(vec.y),
		t(vec.z),
	};
	#undef t
}

int main(const int argc, const char** argv)
{
	if(argc < 3 || argc > 5)
	{
		std::cerr << "parameters: <input filename> <output filename.png> [iterations (4)] [darken amount (0)]\n";
		return EXIT_FAILURE;
	}

	int l = (argc > 3) ? std::stoi(argv[3]) : 4;
	if(argc > 4)
	{
		darken_amount = std::stod(argv[4]);
	}
	if(l < 0)
	{
		l = -l;
		neg = true;
	}
	limit = (l > 32) ? 32 : l;

	std::unique_ptr<uint8_t[]> file_data;
	std::size_t file_size;
	{
		std::ifstream instream(argv[1], std::ifstream::ate | std::ifstream::binary);
		file_size = static_cast<std::size_t>(instream.tellg());
		instream.seekg(0);
		if(file_size == 0)
		{
			std::cerr << "error loading image: the file is empty\n";
			return EXIT_FAILURE;
		}
		file_data = std::make_unique<uint8_t[]>(file_size);
		instream.read(reinterpret_cast<char*>(file_data.get()), static_cast<std::streamsize>(file_size));
	}

	std::unique_ptr<uint8_t[], void (*)(uint8_t*)> data;
	int width, height;

	if(file_size > 11
	&& file_data[ 8] == 'W'
	&& file_data[ 9] == 'E'
	&& file_data[10] == 'B'
	&& file_data[11] == 'P')
	{
		const int status = WebPGetInfo(file_data.get(), file_size, &width, &height) << '\n';
		if(status == 0)
		{
			std::cerr << "error loading image\n";
			return EXIT_FAILURE;
		}
		data = std::unique_ptr<uint8_t[], void (*)(uint8_t*)>
		(
			WebPDecodeRGB(file_data.get(), file_size, nullptr, nullptr),
			WebPFree
		);
	}
	else
	{
		int n;
		data = std::unique_ptr<uint8_t[], void (*)(uint8_t*)>
		(
			stbi_load_from_memory(file_data.get(), file_size, &width, &height, &n, 3),
			stbi_image_free
		);
		if(data == nullptr)
		{
			std::cerr << "error loading image: " << stbi_failure_reason() << "\n";
			return EXIT_FAILURE;
		}
	}

	if(l != 0)
	for(int y = 0; y < height; ++y)
	{
		size_t row = y * width * 3;
		for(int x = 0; x < width; ++x)
		{
			size_t col = x * 3;
			glm::ivec3 color
			{
				data[row + col    ],
				data[row + col + 1],
				data[row + col + 2],
			};
			// when all components are the same, the color does not change, which makes the output look bad
			if(color.r == color.g && color.g == color.b)
			{
				// note: it does not really matter which component is changed
				if(color.r == 255)
				{
					color[x % 3] -= 1;
				}
				else
				{
					color[x % 3] += 1;
				}
			}
			auto pixel = vec_to_pixel(neutralize(pixel_to_vec(color)));
			data[row + col    ] = pixel[0];
			data[row + col + 1] = pixel[1];
			data[row + col + 2] = pixel[2];
		}
	}

	stbi_write_png(argv[2], width, height, 3, data.get(), 0);

	std::cout << "success (if you see this, something fucked up)\n";
	return EXIT_SUCCESS;
}
