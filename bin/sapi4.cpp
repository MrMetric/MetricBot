// Based on https://github.com/TETYYS/SAPI4, which uses the MIT license

#define WIN32_LEAN_AND_MEAN

#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <limits>
#include <stdint.h>
#include <string>

#include <windows.h>
#include <mmsystem.h>
#include <initguid.h>
#include <objbase.h>
#include <objerror.h>
#include <ole2ver.h>

#include "speech.h"

using std::cerr;
using std::cout;

class CTestNotify : public ITTSNotifySink
{
public:
	CTestNotify();

	// IUnkown members that delegate to m_punkOuter
	// Non-delegating object IUnknown
	STDMETHODIMP         QueryInterface (REFIID, LPVOID FAR *);
	STDMETHODIMP_(ULONG) AddRef(void);
	STDMETHODIMP_(ULONG) Release(void);

	// ITTSNotifySink
	STDMETHOD (AttribChanged)  (DWORD);
	STDMETHOD (AudioStart)     (QWORD);
	STDMETHOD (AudioStop)      (QWORD);
	STDMETHOD (Visual)         (QWORD, CHAR, CHAR, DWORD, PTTSMOUTH);

	bool is_done;
};


PIAUDIOFILE		gpIAF = nullptr;


CTestNotify::CTestNotify()
:
	is_done(false)
{
}

STDMETHODIMP CTestNotify::QueryInterface
(
	REFIID riid,
	LPVOID* ppv
)
{
	*ppv = nullptr;

	// always return our IUnknown for IID_IUnknown
	if(IsEqualIID(riid, IID_IUnknown) || IsEqualIID(riid,IID_ITTSNotifySink))
	{
		*ppv = this;
		return S_OK;
	}

	// otherwise, can't find
	return ResultFromScode(E_NOINTERFACE);
}

STDMETHODIMP_(ULONG) CTestNotify::AddRef()
{
	return 1;
}

STDMETHODIMP_(ULONG) CTestNotify::Release()
{
	return 1;
}

STDMETHODIMP CTestNotify::AttribChanged(DWORD dwAttribID)
{
	return NOERROR;
}

STDMETHODIMP CTestNotify::AudioStart(QWORD qTimeStamp)
{
	return NOERROR;
}

STDMETHODIMP CTestNotify::AudioStop(QWORD qTimeStamp)
{
	gpIAF->Flush();
	is_done = TRUE;

	return NOERROR;
}

STDMETHODIMP CTestNotify::Visual
(
	QWORD qTimeStamp,
	CHAR cIPAPhoneme,
	CHAR cEnginePhoneme,
	DWORD dwHints,
	PTTSMOUTH pTTSMouth
)
{
	return NOERROR;
}

PITTSCENTRAL FindAndSelect(TTSMODEINFO& mode_info)
{
	PITTSFIND pITTSFind;
	if(FAILED(CoCreateInstance(CLSID_TTSEnumerator, nullptr, CLSCTX_ALL, IID_ITTSFind, reinterpret_cast<void**>(&pITTSFind))))
	{
		return nullptr;
	}

	TTSMODEINFO ttsResult;
	if(FAILED(pITTSFind->Find(&mode_info, nullptr, &ttsResult)))
	{
		goto failRelease;
	}

	if(strcmp(ttsResult.szModeName, mode_info.szModeName) != 0)
	{
		goto failRelease;
	}

	if(FAILED(CoCreateInstance(CLSID_AudioDestFile, nullptr, CLSCTX_ALL, IID_IAudioFile, reinterpret_cast<void**>(&gpIAF))))
	{
		goto failRelease;
	}

	PITTSCENTRAL pITTSCentral;
	if(FAILED(pITTSFind->Select(ttsResult.gModeID, &pITTSCentral, gpIAF)))
	{
		goto failRelease;
	}

	pITTSFind->Release();
	return pITTSCentral;

failRelease:
	pITTSFind->Release();
	return nullptr;
}

bool BeginOLE()
{
	const DWORD dwVer = CoBuildVersion();
	if(rmm != HIWORD(dwVer))
	{
		return false;
	}
	if(FAILED(CoInitialize(nullptr)))
	{
		return false;
	}
	return true;
}

int main(const int argc, char** argv)
{
	if(argc <= 5)
	{
		cerr << "syntax: " << argv[0] << " <voice: string> <pitch: int> <speed: int> <text: string> <outpath: file path>\n";
		return EXIT_FAILURE;
	}
	if(!BeginOLE())
	{
		cerr << "failed to initialize OLE stuff\n";
		return EXIT_FAILURE;
	}

	const std::string s_voice = argv[1];
	const std::string s_pitch = argv[2];
	const std::string s_speed = argv[3];
	const std::string s_text = argv[4];
	const std::string s_outpath = argv[5];

	TTSMODEINFO ModeInfo;
	if(s_voice.size() > sizeof(ModeInfo.szModeName))
	{
		cerr << "invalid voice\n";
		return EXIT_FAILURE;
	}
	std::fill_n(reinterpret_cast<char*>(&ModeInfo), sizeof(ModeInfo), 0);
	std::copy(s_voice.cbegin(), s_voice.cend(), ModeInfo.szModeName);

	// list voices
	/*
	PITTSENUM pITTSEnum;
	if(FAILED(CoCreateInstance(CLSID_TTSEnumerator, nullptr, CLSCTX_ALL, IID_ITTSEnum, reinterpret_cast<void**>(&pITTSEnum))))
	{
		return EXIT_FAILURE;
	}
	TTSMODEINFO nTTSInfo;
	while(!pITTSEnum->Next(1, &nTTSInfo, nullptr))
	{
		cout << nTTSInfo.szModeName << '\n';
	}
	pITTSEnum->Release();
	*/

	PITTSCENTRAL pITTSCentral = FindAndSelect(ModeInfo);
	if(pITTSCentral == nullptr)
	{
		cerr << "voice not found\n";
		return EXIT_FAILURE;
	}

	gpIAF->RealTimeSet(std::numeric_limits<WORD>::max());

	PITTSATTRIBUTES gpITTSAttributes;
	if(FAILED(pITTSCentral->QueryInterface(IID_ITTSAttributes, reinterpret_cast<void**>(&gpITTSAttributes))))
	{
		cerr << "query interface failed\n";
		return EXIT_FAILURE;
	}

	const uint16_t pitch = std::stoul(s_pitch);
	const uint32_t speed = std::stoul(s_speed);

	WORD default_pitch, min_pitch, max_pitch;
	gpITTSAttributes->PitchGet(&default_pitch);
	gpITTSAttributes->PitchSet(TTSATTR_MINPITCH);
	gpITTSAttributes->PitchGet(&min_pitch);
	gpITTSAttributes->PitchSet(TTSATTR_MAXPITCH);
	gpITTSAttributes->PitchGet(&max_pitch);

	DWORD default_speed, min_speed, max_speed;
	gpITTSAttributes->SpeedGet(&default_speed);
	gpITTSAttributes->SpeedSet(TTSATTR_MINSPEED);
	gpITTSAttributes->SpeedGet(&min_speed);
	gpITTSAttributes->SpeedSet(TTSATTR_MAXSPEED);
	gpITTSAttributes->SpeedGet(&max_speed);

	bool bad_args = false;
	if(pitch < min_pitch || pitch > max_pitch)
	{
		cerr << "pitch out of range\n";
		bad_args = true;
	}
	if(speed < min_speed || speed > max_speed)
	{
		cerr << "speed out of range\n";
		bad_args = true;
	}
	if(bad_args)
	{
		cerr << "note: pitch range is [" << min_pitch << ", " << max_pitch << "]\n";
		cerr << "note: speed range is [" << min_speed << ", " << max_speed << "]\n";
		cerr << "note: default pitch is " << default_pitch << '\n';
		cerr << "note: default speed is " << default_speed << '\n';
		return EXIT_FAILURE;
	}

	CTestNotify gNotify;
	DWORD dwRegKey;
	pITTSCentral->Register(&gNotify, IID_ITTSNotifySink, &dwRegKey);

	WCHAR wszFile[999];
	MultiByteToWideChar(CP_UTF8, 0, s_outpath.data(), -1, wszFile, sizeof(wszFile) / sizeof(WCHAR));
	if(gpIAF->Set(wszFile, 1))
	{
		cerr << "file set failed\n";
		return 1;
	}

	gpITTSAttributes->PitchSet(pitch);
	gpITTSAttributes->SpeedSet(speed);

	SDATA data;
	data.dwSize = s_text.size() + 1;
	data.pData = const_cast<char*>(s_text.data()); // I hope this is safe
	pITTSCentral->AudioReset();
	pITTSCentral->TextData(CHARSET_TEXT, TTSDATAFLAG_TAGGED, data, nullptr, IID_ITTSBufNotifySink);

	BOOL fGotMessage;
	MSG msg;
	while((fGotMessage = GetMessage(&msg, nullptr, 0, 0)) != 0
	      && fGotMessage != -1
	      && !gNotify.is_done)
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	gpIAF->Flush();
	gpIAF->Release();
	gpITTSAttributes->Release();
	pITTSCentral->Release();

	CoUninitialize();

	return EXIT_SUCCESS;
}



