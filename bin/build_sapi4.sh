#!/bin/sh
set -eu -o pipefail

i686-w64-mingw32-g++ \
	-static \
	-static-libgcc \
	-static-libstdc++ \
	-Wall \
	-Wextra \
	-Werror=format \
	-Werror=old-style-cast \
	sapi4.cpp \
	-lole32 \
	-luser32 \
	-lws2_32 \
	-luuid \
	-O2 \
	-s \
	-o sapi4.exe
