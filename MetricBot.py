#!/usr/bin/env python3
import base64
import codecs
import datetime
import errno
import inspect
import json
import logging
import math
import os
import random
import re
import subprocess
import sys
import time
import traceback
import unicodedata
from enum import Enum

import emoji
import requests
import pint
import pysynth_b
import signal
import sqlite3

import telegram.ext
from telegram import (
	InlineQueryResultArticle,
	InputTextMessageContent,
)
from telegram.ext import (
	CallbackQueryHandler,
	CommandHandler,
	InlineQueryHandler,
	MessageHandler,
)

START_TIME = time.time()

'''
def sigint_handler(signum, frame):
	db.commit()
	db.close()
	print("cleanup done")
	exit(0)

signal.signal(signal.SIGINT, sigint_handler)
'''

class ShitstainType(Enum):
	TEXT = 1
	VOICE = 2
	PHOTO = 3

class Shitstain:
	def __init__(self, type, data):
		self.type = type
		self.data = data

class CommandError(Exception):
	pass

def make_temp_filename():
	return 'temp/{:.99}'.format(time.time())

def makedirs(path):
	try:
		os.makedirs(path)
	except OSError as exception:
		if exception.errno != errno.EEXIST:
			raise

def read_file(path: str) -> str:
	with open(path, 'r') as f:
		return f.read()

def log(text: str, write: bool = True) -> None:
	text = '[{}] {}'.format(datetime.datetime.now(), text)
	print(text)
	if write:
		with open('MetricBot.log', 'a') as f:
			f.write(text + '\n')

def get_user_display_name(user) -> str:
	if user.first_name and user.last_name:
		return user.first_name + ' ' + user.last_name
	if user.first_name:
		return user.first_name
	if user.last_name:
		return user.last_name
	if user.username:
		return '@' + user.username
	return 'user' + str(user.id)

CID_DANK = -561247
CID_SHITLORDS = -1001066824407
CID_RURU_R34 = -1001363382762
NO_BOTNET = [
	CID_DANK,
	-144687054,
]
def log_message(bot, m) -> None:
	seen_dates[m.from_user.username] = m.date
	if m.chat_id not in known_chats:
		known_chats[m.chat_id] = m.chat
	text = None
	if m.text:
		text = m.text
	else:
		botnet = True
		if m.audio:
			text = '<Audio: {}>'.format(m.audio.file_id)
		elif m.document:
			text = '<Document: {}; {}>'.format(m.document.file_id, m.document.file_name)
		elif m.sticker:
			text = '<Sticker: {}>'.format(m.sticker.file_id)
		elif m.video:
			text = '<Video: {}>'.format(m.video.file_id)
		elif m.photo:
			text = '<Photo: {}>'.format(largest_photo(m.photo))
		elif m.voice:
			text = '<Voice: {}>'.format(m.voice.file_id)
		else:
			botnet = False
			text = repr(m)
		#if botnet and (m.chat_id not in NO_BOTNET):
		#	bot.forward_message(chat_id='@MetricBotnet', from_chat_id=m.chat_id, message_id=m.message_id)
	log('[{}:{}:{}] {} {} (@{}): {}'.format(m.chat.type, m.chat_id, m.chat.title, m.from_user.first_name, m.from_user.last_name, m.from_user.username, text))

def send_message(bot, chat_id, text: str, parse_mode=None, reply_to_message_id=None) -> None:
	if text:
		m = bot.send_message(chat_id, text, parse_mode=parse_mode, reply_to_message_id=reply_to_message_id)
		log_message(bot, m)

def send_voice(bot, chat_id, file, caption=None, reply_to_message_id=None):
	# TODO
	pass

atname_lower = None

start_date = int(time.time())
COOL_CHATS = [
	CID_DANK,
]
SHITPOST_HARDER = []
known_chats = {}
ADMINS = [
	132410182, # me
]
COMMAND_WHITELIST = {
	CID_RURU_R34: [
		'convert',
		'help',
		'e621',
		'e621id',
		'pony',
		'ponyid',
		'ponyshit',
		'ponytop',
		'worthlessshit',
	],
}
COMMAND_BLACKLIST = {
}

#re_convert = re.compile(r'(\d+(?:\.\d+?)?)\s+([^ ]+)\s+to\s+([^ ]+)')
re_convert = re.compile(r'^(\d+(?:\.\d+)?)\s+(.+)\s+to\s+(.+)$')
ureg = pint.UnitRegistry()
ureg.load_definitions('units')

faggot = [m for m in read_file('faggot').split('\n') if m != '']

seen_dates = {}

def convert_stuff(value: float, unit1: str, unit2: str) -> str:
	unit1_u = None
	try:
		unit1_u = ureg.parse_expression(unit1)
	except pint.UndefinedUnitError as e:
		return str(e)

	unit2_u = None
	try:
		unit2_u = ureg.parse_expression(unit2)
	except pint.UndefinedUnitError as e:
		return str(e)

	unit1_u = value * unit1_u
	answer = None
	try:
		answer = unit1_u.to(unit2_u)
	except pint.DimensionalityError as e:
		return str(e)
	return '{} = {}'.format(unit1_u, answer)

def command_convert(bot, update, text: str):
	'''
	Converts between units ::DD
	Syntax: /convert <amount1> <unit1> to <unit2>
	Note: units can be pluralized
	'''
	convert_match = re_convert.match(text)
	if convert_match:
		unit1 = convert_match.group(2)
		unit2 = convert_match.group(3)
		value = None
		value = float(convert_match.group(1)) # is it possible for this to fail? (ValueError, TypeError)
		return convert_stuff(value, unit1, unit2)
	return 'invalid syntax'

def send_image(bot, m, path, lossless: bool) -> None:
	if lossless:
		log_message(bot, bot.send_document(chat_id=m.chat_id, document=open(path, 'rb'), reply_to_message_id=m.message_id))
	else:
		log_message(bot, bot.send_photo(chat_id=m.chat_id, photo=open(path, 'rb'), reply_to_message_id=m.message_id))

# from MisterChannelBot
def save_url(url, filename):
	if os.path.isfile(filename):
		log('skipped saving ' + url + ' to ' + filename)
		return
	log('saving ' + url + ' to ' + filename)
	r = requests.get(url)
	if r.status_code != 200:
		raise RuntimeError('got status code {} for {}'.format(r.status_code, url))
	with open(filename, 'wb') as f:
		f.write(r.content)

def send_image_or_video(bot, m, url: str, caption: str) -> None:
	print("send_image_or_video: {} {}".format(url, caption))
	if url.endswith('.mp4') or url.endswith('.gif'):
		log_message(bot, bot.send_document(chat_id=m.chat_id, document=url, caption=caption, reply_to_message_id=m.message_id))
	else:
		try:
			log_message(bot, bot.send_photo(chat_id=m.chat_id, photo=url, caption=caption, reply_to_message_id=m.message_id))
		except telegram.error.BadRequest as e1: # ??? it does this randomly (Bad Request: wrong file identifier/HTTP URL specified)
			log("unable to post {}: {}".format(url, e1))
			path = "temp/" + url.replace("/", "_")
			save_url(url, path)
			log_message(bot, bot.send_photo(chat_id=m.chat_id, photo=open(path, 'rb'), caption=caption, reply_to_message_id=m.message_id))

def url_file_size(url: str):
	r = requests.head(url)
	if r.status_code != 200:
		return None
	return int(r.headers['content-length'])


BOORU_KEY = {
	"derpibooru.org": read_file('DERPIBOORU_KEY'),
	"memebooru.com" : read_file('MEMEBOORU_KEY' ),
	"ponerpics.org" : read_file('PONERPICS_KEY' ),
	"ponybooru.org" : read_file('PONYBOORU_KEY' ),
	"twibooru.org"  : read_file('TWIBOORU_KEY'  ),
}
BOORU_MAX = {
	"derpibooru.org": 2624659,
	"memebooru.com" : 41     ,
	"ponerpics.org" : 6177396,
	"ponybooru.org" : 2858647,
	"twibooru.org"  : 2467334,
}
BOORU_URL_ID = {
	"derpibooru.org": "https://{}/api/v1/json/images/{}",
	"memebooru.org" : "https://{}/api/v1/json/images/{}",
	"ponybooru.org" : "https://{}/api/v1/json/images/{}",
	"ponerpics.org" : "https://{}/api/v1/json/images/{}",
	"twibooru.org"  : "https://{}/{}.json",
}
BOORU_URL_QUERY = {
	"derpibooru.org": "https://{}/api/v1/json/search/images",
	"memebooru.org" : "https://{}/api/v1/json/search/images",
	"ponybooru.org" : "https://{}/api/v1/json/search/images",
	"ponerpics.org" : "https://{}/api/v1/json/search/images",
	"twibooru.org"  : "https://{}/search.json",
}

def add_site_to_url(site, url):
	if url[0] == '/':
		return "https://{}{}".format(site, url)
	return url

def get_pony_image_url(site, image):
	if "representations" not in image:
		return None

	if "mp4" in image['representations']:
		return add_site_to_url(site, image['representations']['mp4'])

	image_url = add_site_to_url(site, image['representations']['large'])
	if image_url.endswith('.gif'):
		return add_site_to_url(site, image['representations']['full'])
	if url_file_size(image_url) > 5000000:
		return add_site_to_url(site, image['representations']['medium'])
	return image_url

def ponyid_info(image, site: str) -> str:
	if ("deletion_reason" in image) and (image["deletion_reason"] is not None):
		return "That image has been deleted. Reason: " + image["deletion_reason"]
	if ("duplicate_of" in image) and (image["duplicate_of"] is not None):
		caption = ponyid_info(str(image["duplicate_of"]), site)
		return "That image is a duplicate of #{}\n{}".format(image["duplicate_of"], caption)
	info = 'upvotes: {upvotes}; downvotes: {downvotes}; score: {score}; faves: {faves}\n'.format_map(image)
	#r += "tags: " + image["tags"] + '\n'
	image_id = image["id"]
	info += 'https://{}/{}'.format(site, image_id)
	if (image_id == 736115) and (site == "derpibooru.org"):
		info = 'Aryanne to the rescue!\n' + info
	return info

def do_ponyid_(text: str, site: str):
	try:
		image_id = int(text)
	except ValueError:
		return "that ain't a base ten integer, " + random.choice(faggot)

	if image_id < 0:
		return random.choice([
			'what the fuck are you trying to do',
			'what are you playing at, penisguzzler?',
			'you can fuck right off',
			'pls no crashy',
		])

	print("getting ID {} from {}".format(image_id, site))

	headers = {
		"User-Agent": "MetricBot (by iloveportalz0r)",
	}
	r = requests.get(BOORU_URL_ID[site].format(site, image_id), headers=headers)
	if r.status_code != 200:
		return "do_ponyid_ got status code: {}".format(r.status_code)
	return r.json()["image"]

def do_ponyid(text: str, site: str):
	if text == '':
		return 'give me something to work with here, ' + random.choice(faggot)

	text = text.strip().lstrip('0')
	if text == '':
		text = '0'
	image = do_ponyid_(text, site)
	if isinstance(image, str):
		return image
	url = get_pony_image_url(site, image)
	caption = ponyid_info(image, site)
	if url is None:
		return caption
	return url, caption

pony_page_count = {}
def get_pony_page(query, url: str, params):
	r = requests.get(url, params=params)
	if r.status_code != 200:
		raise CommandError("get_pony_page got status code: {}".format(r.status_code))
	q = r.json()
	total = q["total"]
	if total == 0:
		raise CommandError('No results')
	if "images" in q:
		results = q["images"]
	else:
		results = q["search"]
	if len(results) == 0:
		raise CommandError("some shit fucked up: len(results) == 0")
	page_count = math.ceil(total / len(results))
	pony_page_count[query] = page_count
	return results, total, page_count

def do_pony(text: str, site: str):
	query = text.lower()
	url = BOORU_URL_QUERY[site].format(site)
	params = {
		'key': BOORU_KEY[site],
		'q': query,
	}
	r = None
	if query in pony_page_count:
		page_count = pony_page_count[query]
	else:
		results, total, page_count = get_pony_page(query, url, params)
	page = random.randint(1, page_count)
	if page != 1 or r is None:
		params["page"] = str(page)
		results, total, page_count = get_pony_page(query, url, params)
	image_i = random.randint(1, len(results))
	image = results[image_i - 1]

	image_url = get_pony_image_url(site, image)
	caption = 'Result {} of {} for query: {}\n'.format((page - 1)*50 + image_i, total, text) + ponyid_info(image, site)
	return image["id"], image_url, caption
def do_ponytop(text: str, sd: str, site: str):
	if text == '':
		query = 'safe || !safe'
	else:
		query = text.lower()
	url = 'https://{}/api/v1/json/search/images'.format(site)
	params = {
		'key': BOORU_KEY[site],
		'q': query,
		'sf': 'score',
		'sd': sd,
	}
	results, total, page_count = get_pony_page(query, url, params)
	image = results[0]

	image_url = get_pony_image_url(site, image)
	if sd == 'desc':
		top = 'Top'
	else:
		top = 'Shittiest'
	if text == '':
		caption = top + ' pony image\n'
	else:
		caption = top + ' result for query: {}\n'.format(text)
	caption += ponyid_info(image, site)
	return image['id'], image_url, caption


def command_ponyid(bot, update, text: str):
	'''
	This shit will grab a cancerous pony image from Ponybooru
	Syntax: /ponyid <id>
	'''
	if text == '':
		return 'give me an ID, faget'
	info = do_ponyid(text, "ponybooru.org")
	if isinstance(info, str):
		return info
	url, caption = info
	send_image_or_video(bot, update.message, url, caption)

def command_pony(bot, update, text: str):
	'''
	Syntax: /pony [query]
	This searches the query on Ponybooru and returns a random result. If no query is given, it returns a random image.
	'''
	#if update.message.chat_id == CID_SHITLORDS:
	#	return
	if text == '':
		url, caption = do_ponyid(str(random.randint(0, BOORU_MAX["ponybooru.org"])), "ponybooru.org")
	else:
		image_id, url, caption = do_pony(text.strip(), "ponybooru.org")
	send_image_or_video(bot, update.message, url, caption)

def command_catpony(bot, update, text: str):
	query = 'safe, (behaving like a cat || twicat)'
	if text != '':
		query += ', ' + text
	image_id, url, caption = do_pony(query, "ponybooru.org")
	send_image_or_video(bot, update.message, url, caption)

def command_ponytop(bot, update, text: str):
	image_id, url, caption = do_ponytop(text, "desc", "ponybooru.org")
	send_image_or_video(bot, update.message, url, caption)

def command_ponyshit(bot, update, text: str):
	image_id, url, caption = do_ponytop(text, "asc", "ponybooru.org")
	send_image_or_video(bot, update.message, url, caption)


def command_ponerid(bot, update, text: str):
	'''
	This shit will grab a cancerous pony image from Ponerpics
	Syntax: /ponerid <id>
	'''
	if text == '':
		return 'give me an ID, faget'
	info = do_ponyid(text, "ponerpics.org")
	if isinstance(info, str):
		return info
	url, caption = info
	send_image_or_video(bot, update.message, url, caption)

def command_poner(bot, update, text: str):
	'''
	Syntax: /poner [query]
	This searches the query on Ponerpics and returns a random result. If no query is given, it returns a random image.
	'''
	#if update.message.chat_id == CID_SHITLORDS:
	#	return
	if text == '':
		url, caption = do_ponyid(str(random.randint(0, BOORU_MAX["ponerpics.org"])), "ponerpics.org")
	else:
		image_id, url, caption = do_pony(text.strip(), "ponerpics.org")
	send_image_or_video(bot, update.message, url, caption)

def command_catponer(bot, update, text: str):
	query = 'safe, (behaving like a cat || twicat)'
	if text != '':
		query += ', ' + text
	image_id, url, caption = do_pony(query, "ponerpics.org")
	send_image_or_video(bot, update.message, url, caption)

def command_ponertop(bot, update, text: str):
	image_id, url, caption = do_ponytop(text, "desc", "ponerpics.org")
	send_image_or_video(bot, update.message, url, caption)

def command_ponershit(bot, update, text: str):
	image_id, url, caption = do_ponytop(text, "asc", "ponerpics.org")
	send_image_or_video(bot, update.message, url, caption)


def command_derpid(bot, update, text: str):
	'''
	This shit will grab a cancerous pony image from Derpibooru
	Syntax: /derpid <id>
	'''
	if text == '':
		return 'give me an ID, faget'
	info = do_ponyid(text, "derpibooru.org")
	if isinstance(info, str):
		return info
	url, caption = info
	send_image_or_video(bot, update.message, url, caption)

def command_derpiid(bot, update, text: str):
	return command_derpid(bot, update, text)

def command_derpi(bot, update, text: str):
	'''
	Syntax: /derpi [query]
	This searches the query on Derpibooru and returns a random result. If no query is given, it returns a random image.
	'''
	#if update.message.chat_id == CID_SHITLORDS:
	#	return
	if text == '':
		url, caption = do_ponyid(str(random.randint(0, BOORU_MAX["derpibooru.org"])), "derpibooru.org")
	else:
		image_id, url, caption = do_pony(text.strip(), "derpibooru.org")
	send_image_or_video(bot, update.message, url, caption)

def command_derpitop(bot, update, text: str):
	image_id, url, caption = do_ponytop(text, "desc", "derpibooru.org")
	send_image_or_video(bot, update.message, url, caption)

def command_derpishit(bot, update, text: str):
	image_id, url, caption = do_ponytop(text, "asc", "derpibooru.org")
	send_image_or_video(bot, update.message, url, caption)


#def command_memeid(bot, update, text: str):
#	'''
#	This shit will grab a cancerous pony image from Derpibooru
#	Syntax: /memeid <id>
#	'''
#	if text == '':
#		return 'give me an ID, faget'
#	info = do_ponyid(text, "memebooru.org")
#	if isinstance(info, str):
#		return info
#	url, caption = info
#	send_image_or_video(bot, update.message, url, caption)
#
#def command_meme(bot, update, text: str):
#	'''
#	Syntax: /meme [query]
#	This searches the query on Derpibooru and returns a random result. If no query is given, it returns a random image.
#	'''
#	#if update.message.chat_id == CID_SHITLORDS:
#	#	return
#	if text == '':
#		url, caption = do_ponyid(str(random.randint(0, BOORU_MAX["memebooru.org"])), "memebooru.org")
#	else:
#		image_id, url, caption = do_pony(text.strip(), "memebooru.org")
#	send_image_or_video(bot, update.message, url, caption)


def command_twid(bot, update, text: str):
	'''
	This shit will grab a cancerous pony image from Twibooru
	Syntax: /twid <id>
	'''
	if text == '':
		return 'give me an ID, faget'
	info = do_ponyid(text, "twibooru.org")
	if isinstance(info, str):
		return info
	url, caption = info
	send_image_or_video(bot, update.message, url, caption)

def command_twiid(bot, update, text: str):
	'''
	This shit will grab a cancerous pony image from Twibooru
	Syntax: /twiid <id>
	'''
	return command_twid(bot, update, text)

def command_twi(bot, update, text: str):
	'''
	Syntax: /twi [query]
	This searches the query on Twibooru and returns a random result. If no query is given, it returns a random image.
	'''
	if text == '':
		url, caption = do_ponyid(str(random.randint(0, BOORU_MAX["twibooru.org"])), "twibooru.org")
	else:
		image_id, url, caption = do_pony(text.strip(), "twibooru.org")
	send_image_or_video(bot, update.message, url, caption)

def command_twitop(bot, update, text: str):
	image_id, url, caption = do_ponytop(text, "desc", "twibooru.org")
	send_image_or_video(bot, update.message, url, caption)

def command_twishit(bot, update, text: str):
	image_id, url, caption = do_ponytop(text, "asc", "twibooru.org")
	send_image_or_video(bot, update.message, url, caption)


def get_e621_image_url(image):
	image_url = image['file_url']
	if url_file_size(image_url) > 5000000: # TODO: check image['file_size']
		return image['preview_url']
	return image_url

def e621id_info(image) -> str:
	if 'delreason' in image:
		return 'That post has been deleted. Reason: ' + image['delreason']
	info = 'score: {score}; faves: {fav_count}\n'.format_map(image)
	image_id = image['id']
	info += 'https://e621.net/post/show/{}'.format(image_id)
	return info

def do_e621id_(text: str):
	if text == '':
		return 'give me something to work with here, ' + random.choice(faggot)

	try:
		image_id = int(text)
	except ValueError:
		return "that ain't a base ten number, " + random.choice(faggot)

	if image_id < 0:
		return random.choice([
			'ur a negative nigga',
			'your dick is as short as that minus sign',
			'pls no crashy',
		])

	headers = {
		'User-Agent': 'MetricBot (by MrMetric)'
	}
	r = requests.get('https://e621.net/post/show/{}.json'.format(image_id), headers=headers)
	if r.status_code != 200:
		return 'do_e621id_ got status code: {}'.format(r.status_code)
	image = r.json()
	return image

def do_e621id(text: str):
	text = text.strip().lstrip('0')
	image = do_e621id_(text)
	if isinstance(image, str):
		return image
	url = get_e621_image_url(image)
	caption = e621id_info(image)
	if url is None:
		return caption
	return url, caption

def command_e621id(bot, update, text: str):
	'''
	This shit will grab a cancerous image from e621
	Syntax: /e621id <id>
	'''
	if text == '':
		return 'give me an ID, faget'
	info = do_e621id(text)
	if isinstance(info, str):
		return info
	url, caption = info
	send_image_or_video(bot, update.message, url, caption)

def command_e621(bot, update, text: str):
	headers = {
		'User-Agent': 'MetricBot (by MrMetric)'
	}
	query = text.lower()
	params = {
		'limit': '320',
		'tags': query,
	}
	r = requests.get('https://e621.net/post/index.json', headers=headers, params=params)
	# TODO: randomly attempt loading more pages (e621 does not say how many results there are)
	if r.status_code != 200:
		return 'command_e621 got status code: {}'.format(r.status_code)
	results = r.json()
	image = random.choice(results)
	url = get_e621_image_url(image)
	caption = e621id_info(image)
	send_image_or_video(bot, update.message, url, caption)


def text_subst(char_map, text: str, cases=False):
	if text == '':
		return text_subst(char_map, 'you are doing it wrong, ' + random.choice(faggot), cases)

	output = ''
	for c in text:
		if c in char_map:
			output += char_map[c]
		elif cases and (c.lower() in char_map):
			output += char_map[c.lower()].upper()
		else:
			output += c
	return output

wide_map = {
	'!': '！',
	'"': '＂',
	'#': '＃',
	'$': '＄',
	'%': '％',
	'&': '＆',
	"'": '＇',
	'(': '（',
	')': '）',
	'*': '＊',
	'+': '＋',
	',': '，',
	'-': '－',
	'.': '．',
	'/': '／',
	'0': '０',
	'1': '１',
	'2': '２',
	'3': '３',
	'4': '４',
	'5': '５',
	'6': '６',
	'7': '７',
	'8': '８',
	'9': '９',
	':': '：',
	';': '；',
	'<': '＜',
	'=': '＝',
	'>': '＞',
	'?': '？',
	'@': '＠',
	'A': 'Ａ',
	'B': 'Ｂ',
	'C': 'Ｃ',
	'D': 'Ｄ',
	'E': 'Ｅ',
	'F': 'Ｆ',
	'G': 'Ｇ',
	'H': 'Ｈ',
	'I': 'Ｉ',
	'J': 'Ｊ',
	'K': 'Ｋ',
	'L': 'Ｌ',
	'M': 'Ｍ',
	'N': 'Ｎ',
	'O': 'Ｏ',
	'P': 'Ｐ',
	'Q': 'Ｑ',
	'R': 'Ｒ',
	'S': 'Ｓ',
	'T': 'Ｔ',
	'U': 'Ｕ',
	'V': 'Ｖ',
	'W': 'Ｗ',
	'X': 'Ｘ',
	'Y': 'Ｙ',
	'Z': 'Ｚ',
	'[': '［',
	'\\': '＼',
	']': '］',
	'^': '＾',
	'_': '＿',
	'`': '｀',
	'a': 'ａ',
	'b': 'ｂ',
	'c': 'ｃ',
	'd': 'ｄ',
	'e': 'ｅ',
	'f': 'ｆ',
	'g': 'ｇ',
	'h': 'ｈ',
	'i': 'ｉ',
	'j': 'ｊ',
	'k': 'ｋ',
	'l': 'ｌ',
	'm': 'ｍ',
	'n': 'ｎ',
	'o': 'ｏ',
	'p': 'ｐ',
	'q': 'ｑ',
	'r': 'ｒ',
	's': 'ｓ',
	't': 'ｔ',
	'u': 'ｕ',
	'v': 'ｖ',
	'w': 'ｗ',
	'x': 'ｘ',
	'y': 'ｙ',
	'z': 'ｚ',
	'{': '｛',
	'|': '｜',
	'}': '｝',
	'~': '～',
	#' ': '  ',#'　',
}
def command_wide(bot, update, text: str):
	'''
	ＡＥＳＴＨＥＴＩＣ
	'''
	return text_subst(wide_map, text)

fraktur_map = {
	'A': '𝔄',
	'B': '𝔅',
	'C': 'ℭ',
	'D': '𝔇',
	'E': '𝔈',
	'F': '𝔉',
	'G': '𝔊',
	'H': 'ℌ',
	'I': 'ℑ',
	'J': '𝔍',
	'K': '𝔎',
	'L': '𝔏',
	'M': '𝔐',
	'N': '𝔑',
	'O': '𝔒',
	'P': '𝔓',
	'Q': '𝔔',
	'R': 'ℜ',
	'S': '𝔖',
	'T': '𝔗',
	'U': '𝔘',
	'V': '𝔙',
	'W': '𝔚',
	'X': '𝔛',
	'Y': '𝔜',
	'Z': 'ℨ',
	'a': '𝔞',
	'b': '𝔟',
	'c': '𝔠',
	'd': '𝔡',
	'e': '𝔢',
	'f': '𝔣',
	'g': '𝔤',
	'h': '𝔥',
	'i': '𝔦',
	'j': '𝔧',
	'k': '𝔨',
	'l': '𝔩',
	'm': '𝔪',
	'n': '𝔫',
	'o': '𝔬',
	'p': '𝔭',
	'q': '𝔮',
	'r': '𝔯',
	's': '𝔰',
	't': '𝔱',
	'u': '𝔲',
	'v': '𝔳',
	'w': '𝔴',
	'x': '𝔵',
	'y': '𝔶',
	'z': '𝔷',
}
def command_fraktur(bot, update, text: str):
	'''
	ℭ𝔞𝔫𝔠𝔢𝔯𝔬𝔲𝔰 𝔪𝔞𝔱𝔥 𝔣𝔬𝔫𝔱
	'''
	return text_subst(fraktur_map, text)

flag_map = {
	'a': '🇦',
	'b': '🇧',
	'c': '🇨',
	'd': '🇩',
	'e': '🇪',
	'f': '🇫',
	'g': '🇬',
	'h': '🇭',
	'i': '🇮',
	'j': '🇯',
	'k': '🇰',
	'l': '🇱',
	'm': '🇲',
	'n': '🇳',
	'o': '🇴',
	'p': '🇵',
	'q': '🇶',
	'r': '🇷',
	's': '🇸',
	't': '🇹',
	'u': '🇺',
	'v': '🇻',
	'w': '🇼',
	'x': '🇽',
	'y': '🇾',
	'z': '🇿',
}
def command_flags(bot, update, text: str):
	return text_subst(flag_map, text, True)
def command_flags2(bot, update, text: str):
	return text_subst(flag_map, '​'.join(text), True)

umlaut_map = {
	'a': 'ä',
	'b': 'ḃ',
	'c': 'ċ',
	'd': 'ḋ',
	'e': 'ë',
	'f': 'ḟ',
	'g': 'ġ',
	'h': 'ḧ',
	'i': 'ï',

	'k': 'ĸ',
	'l': 'ł',
	'm': 'ṁ',
	'n': 'ṅ',
	'o': 'ö',
	'p': 'ṗ',

	'r': 'ṙ',
	's': 'ṡ',
	't': 'ẗ',
	'u': 'ü',

	'w': 'ẅ',
	'x': 'ẍ',
	'y': 'ÿ',
	'z': 'ż',
}
def command_rockdots(bot, update, text: str):
	'''
	ṙöċĸḋöẗṡ
	'''
	return text_subst(umlaut_map, text, True)

cancer = read_file('cancer').split(' ')
cancer2 = [m + ' ' for m in read_file('cancer2').split('\n') if m != '']

def generate_cancer(amount):
	return ''.join([random.choice(cancer) for x in range(amount)])

def cancer_number(inpoot, default, min_n, max_n):
	if inpoot == '':
		if default is None:
			return random.randint(min_n, max_n)
		return default

	try:
		n = int(inpoot, 12)
	except ValueError as e:
		raise CommandError('that is not an integer yo uretarded shitlord ' + generate_cancer(8))

	if n < 0:
		raise CommandError('wate a sec r u tryna crash me')
	if (n < min_n) or (n > max_n):
		raise CommandError('fuck off')

	return n

def command_cancer(bot, update, text: str):
	'''
	Gives you cancer
	'''
	amount = cancer_number(text, 3, 1, 16)

	outpoot = ''
	for i in range(amount):
		outpoot += generate_cancer(random.randint(0, 8))
		outpoot += ''.join(c for c in cancer2 if random.randint(0, 999) % 24 == 1)
	return outpoot

def command_emoji(bot, update, text: str):
	'''
	gives you emojicancer 👨🏿🏬🏷🆎😶🇩🇯👎🏼📝
	Syntax: /emoji [amount]
	'''
	amount = cancer_number(text, None, 4, 32)
	return generate_cancer(amount)

def randomize(word, hueness):
	if word == '':
		return randomize('fuck off', hueness)
	if len(set(word)) == 1:
		return randomize('no u', hueness)

	hue = ''
	last = ''
	for i in range(hueness):
		thingy = random.choice(word)
		while thingy == last:
			thingy = random.choice(word)
		last = thingy
		hue += thingy
	return hue

def hueize(text):
	l = len(text)
	hueness = random.randint(l, l * 4)
	return randomize(text, hueness)

def command_hueize(bot, update, text: str):
	return hueize(text)

def command_huehue(bot, update, text: str):
	return hueize('huae')

def command_saturationsaturation(bot, update, text: str):
	return hueize('saturation')

def command_valuevalue(bot, update, text: str):
	return 'fuck off'
def command_lightnesslightness(bot, update, text: str):
	return 'there is darkness within me'

def command_sjw(bot, update, text: str):
	return 'What\'s my gender?🚺🚹❓❓Did you just😱ask me😂😂what my gender👧🏽👦🏽 was❓❗️Oh my god, you are LITERALLY👋🏻 the most IGNORANT💩AND ILLTERATE 📚📚📚❌❌ASS I have EVER seen👀👀👀browsing before😂😂😂😂😂What gives you the RIGHT ✔️✔️ to ask me what my🚺🚹🚺🚹gender is?😂😂😂MHMM? NOTHING❌❌THATS WHAT ❗️❗️❗️❗️❗️❗️❗️❗️❗️Since you\'re SOOOOOOO curious🙇🏼🙇🏼🙇🏼🙇🏼 I\'ll have you know ☝🏼️☝🏼️☝🏼️☝🏼️ that I am a 🚺🚹 G E N D E R F L U I D 🚹🚺 I\'m not gonna sit here ⬇️⬇️ and explain 📢📢 what that is to someone as IGNORANT 😂💩💩 as you, but I\'m gonna tell you anyways ☝🏼️🙇🏼☝🏼️🙇🏼☝🏼️🙇🏼 Being G 🚺 E 🚹 N 🚺 D 🚹 E 🚺 R 🚹 F 🚺 L 🚹 U 🚺 I 🚹 D means that I am both M A L E 🍆💦👦🏽 (cis scum😷😷😷😷😷) and F E M A L E👧🏽👠💄🍑 ❗️❗️❗️❗️ I can swap ⬇️⬆️➡️⬅️ between these genders 👌🏻😂 as I please❗️❗️❗️❗️❗️❗️❗️ You\'re ignorance 😷 is literally OOZING 💦💦💦💦 out 😂😂😷😷😂😷😂😂'

def command_uptime(bot, update, text: str):
	return str(datetime.timedelta(seconds=time.time() - START_TIME))

def command_downtime(bot, update, text: str):
	return 'a small loan of a million seconds'

def command_lefttime(bot, update, text: str):
	return "Ah ain't no damn COMMUNIST!"

def command_righttime(bot, update, text: str):
	return '6 years and 1 day'

def command_centertime(bot, update, text: str):
	return "i got nuthin'"

def command_reichtime(bot, update, text: str):
	return 'HEIL HITLER'

def command_northwesttime(bot, update, text: str):
	return '/kanyewesttime'

def command_kanyewesttime(bot, update, text: str):
	return random.choice([
		'DO NOT LET ME INTO MY ZONE',
		'DO NOT LET ME INTO MY ZONE',
		'DO NOT LET ME INTO MY ZONE',
		'I AM DEFINITELY IN MY ZONE',
	])

def command_southwesttime(bot, update, text: str):
	return 'fuck off'
def command_northeasttime(bot, update, text: str):
	return 'fuck off'
def command_southeasttime(bot, update, text: str):
	return 'fuck off'

def command_flip(bot, update, text: str):
	'''
	get heads or tails
	'''
	return 'uhh… ' + random.choice(['head', 'tail']) + 's?'

def command_flipzone(bot, update, text: str):
	return random.choice([
		'I AM NOT REALLY IN MY ZONE',
		'I AM DEFINITELY IN MY ZONE',
	])

CONSONANT = [
	'b',
	'd',
	'f',
	'g',
	'k',
	'n',
	'p',
	's',
	't',
	'z',
]
VOWEL = [
	'a',
	'e',
	'i',
	'o',
	'u',
	'ae',
	'oe',
]
def command_word(bot, update, text: str):
	'''
	Generates a random piece of shit word
	'''
	wordness = random.randint(2, 6)
	word = random.choice(CONSONANT)
	for i in range(wordness):
		word += random.choice(VOWEL)
		word += random.choice(CONSONANT)
	return word

def command_excel(bot, update, text: str):
	'''
	meme
	'''
	return 'THIS IS NOT WHAT IT FEELS LIKE TO CHEW NEW FIVE GUM'

def command_fortune(bot, update, text: str):
	'''
	$ fortune -ac
	'''
	return str(subprocess.run(['/usr/bin/fortune', '-ac'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout, 'utf-8')

def command_tableflip(bot, update, text: str):
	return '(╯°□°）╯︵ ┻━┻'

def command_unflip(bot, update, text: str):
	return '┬─┬﻿ ノ( ゜-゜ノ)'

def command_rand(bot, update, text: str):
	return str(random.randint(0, 99999999))

def command_help(bot, update, text: str):
	'''
	Gets your dumb self help for a command
	<> denotes a required parameter
	[] denotes an optional parameter
	Syntax: /help <command name>
	'''
	text = text.strip()
	if text == '':
		return 'Syntax: /help <command name>'
	if text not in commands:
		return 'command not found: ' + text
	doc = inspect.getdoc(commands[text])
	if doc is None:
		return 'no help for that command'
	return doc

RMSEXY = read_file('rmsexy.list').split('\n')
def command_rmsexy(bot, update, text: str):
	return 'https://rms.sexy/img/' + random.choice(RMSEXY)

def command_order(bot, update, text: str):
	return 'GCC gcc'

def command_bot(bot, update, text: str):
	return 'yeah whatever'

def command_stop(bot, update, text: str):
	return 'how aboot i stop ur mom'

def command_and_they_dont_stop_coming(bot, update, text: str):
	return '/and_they_dont_stop_coming'
	#return 'Slider, no sliding!'

def command_e(bot, update, text: str):
	return 'e ≈ 2.718281828459045235360287471352662497757247'

def command_tau(bot, update, text: str):
	return 'τ ≈ 6.283185307179586476925286766559'
command_τ = command_tau

def command_pi(bot, update, text: str):
	return 'π ≈ /tau / 2'
command_π = command_pi

def command_gamma(bot, update, text: str):
	return 'γ ≈ 0.57721566490153286060651209'
command_γ = command_gamma

def get_email(id):
	filepath = 'podesta/' + str(id)
	if os.path.isfile(filepath):
		return read_file(filepath)

	url = 'https://wikileaks.org/podesta-emails/get/' + str(id)
	r = requests.get(url)
	if r.status_code != 200:
		return 'got status code {} for email {}'.format(r.status_code, id)
	with open(filepath, 'wb') as f:
		f.write(r.content)
	return r.content

'''
def command_podesta(bot, update, text: str):
	min_id = 1
	max_id = 43524
	id = random.randint(min_id, max_id)
	content = get_email(id)
	#email = parse_email(content)
	return content
'''

def command_anime(bot, update, text: str):
	func = random.choice([
		command_flags2,
		command_fraktur,
		command_rockdots,
		command_wide,
	])
	text = random.choice([
		'anime was a mistake',
		'ANIME WAS A MISTAKE',
	])
	return func(bot, update, text)

def command_telegram(bot, update, text: str):
	return 'telegram my butthole'

def command_discord(bot, update, text: str):
	image_id, url, caption_ = do_pony('discord, safe, score.gt:64', "derpibooru.org")
	caption = 'Do you mean this charming fellow?\n\nhttps://derpibooru.org/{}'.format(image_id)
	send_image_or_video(bot, update.message, url, caption)

def command_skype(bot, update, text: str):
	return random.choice([
		'*Telegram',
		'Skypenet',
		'woo, adware',
	])

def command_lol(bot, update, text: str):
	amount = random.randint(8, 32)
	r = ''
	for i in range(amount):
		r += random.choice(['l', 'o', 'l'])
	return r

def command_holocaust(bot, update, text: str):
	return random.choice([
		'lolocaust',
		'download more gas',
		'*holohoax',
	])

TTS_VOICES = [
	'af',
	'bs',
	'ca',
	'cs',
	'da',
	'de',
	'el',
	'en',
	'en-us',
	'en-sc',
	'en-n',
	'en-rp',
	'en-wm',
	'eo',
	'es',
	'es-la',
	'fi',
	'fr',
	'hr',
	'hu',
	'it',
	'jbo',
	'kn',
	'ku',
	'la',
	'lv',
	'nl',
	'pl',
	'pt',
	'pt-pt',
	'ro',
	'ru',
	'sk',
	'sr',
	'sv',
	'sw',
	'ta',
	'tr',
	'vi',
	'zh',
]
TTS_VARIANTS = [
	'croak',
	'f1',
	'f2',
	'f3',
	'f4',
	'f5',
	'klatt',
	'klatt2',
	'klatt3',
	'klatt4',
	'm1',
	'm2',
	'm3',
	'm4',
	'm5',
	'm6',
	'm7',
	#'whisper',
	#'whisperf',
]
def command_espeak(bot, update, text: str):
	text = text.strip()
	if text == '':
		return 'fuck off'

	m = update.message
	bot.send_chat_action(m.chat_id, 'record_audio')

	voice = ''
	parts = text.split(' ')
	if parts[0] in TTS_VOICES:
		voice = parts[0]
		text = ' '.join(parts[1:])
	else:
		voice = random.choice([
			'en',
			'en-us',
			'en-sc',
			'en-n',
			'en-rp',
			'en-wm',
		])

	variant = '+' + random.choice(TTS_VARIANTS)
	voice += variant
	wpm = random.randint(100, 150)
	filename = make_temp_filename()
	subprocess.run([
		'espeak',
		'-s', str(wpm), # words per minute
		'-v' + voice, # English with Scottish accent
		'-w',
		filename + '.wav',
		text,
	], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
	subprocess.run([
		'ffmpeg',
		'-hide_banner',
		'-i', filename + '.wav',
		'-acodec', 'libopus',
		'-b:a', '128000',
		'-vbr', 'on',
		'-compression_level', '10',
		filename + '.ogg',
	], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
	os.remove(filename + '.wav')

	with open(filename + '.ogg', 'rb') as f:
		log_message(bot, bot.send_voice(m.chat_id, f, caption='voice: {}; wpm: {}'.format(voice, wpm), reply_to_message_id=m.message_id))

def command_espeaklangs(bot, update, text: str):
	return ', '.join(TTS_VOICES)

def command_day(bot, update, text: str):
	text = text.strip().upper()
	if text == '':
		text = 'SYNTAX ERROR'
	return '🔥🔥🔥 ' + text_subst(wide_map, 'DAY OF THE ' + text) + ' 🔥🔥🔥'

def command_gimp(bot, update, text: str):
	return 'best image editor'

def command_photoshop(bot, update, text: str):
	return 'non-free pig disgusting (and shitty anyway)'

def command_allah(bot, update, text: str):
	allah = 'بخور کیربده مامان جندتمامانت خوب میخوره'
	return ''.join([str(w) for w in random.sample(allah, random.randint(len(allah) // 2, len(allah)))])

def command_isis(bot, update, text: str):
	m = update.message
	log_message(bot, bot.send_video(chat_id=m.chat_id, video='BAADAQADEwADfGNIREnNB9LcGciZAg', reply_to_message_id=m.message_id))

def command_b(bot, update, text: str):
	return 'fucking stop it, you ' + random.choice(faggot)

DEUSVOLT = read_file('deusvolt').split('\n')
def command_deusvolt(bot, update, text: str):
	return 'Mike "' + random.choice(DEUSVOLT) + '" Pence'

def command_google(bot, update, text: str):
	'''
	Get results from Google's autocomplete.
	Syntax: /google some dumb query
	If you want to end the query with a space, use %20, like so:
	/google are old%20
	This excludes results that start with 'are older'
	'''
	#https://suggestqueries.google.com/complete/search?client=firefox&q=test%20
	#https://www.google.ca/complete/search?client=firefox&xhr=t&q=test%20
	query = text.replace('%20', ' ').strip()
	if query == '':
		return 'give me a query, faget'
	r = requests.get('https://www.google.ca/complete/search', params={
		'client': 'firefox',
		'xhr': 't',
		'q': query,
	})
	if r.status_code != 200:
		return 'command_google got status code: ' + str(r.status_code)
	results = r.json()[1]
	if len(results) == 0:
		return 'no results from Google autocomplete for query: {}\ntry /yahoo'.format(query)
	return 'Google autocomplete results for: {}\n{}'.format(query, '\n'.join(results))

def command_yahoo(bot, update, text: str):
	'''
	Get results from Yahoo's autocomplete.
	Syntax: /yahoo some dumb query
	If you want to end the query with a space, use %20, like so:
	/yahoo are old%20
	This excludes results that start with 'are older'
	'''
	#https://ca.search.yahoo.com/sugg/gossip/gossip-ca-ura/?output=sd1&command=test%20
	query = text.replace('%20', ' ').strip()
	if query == '':
		return 'give me a query, faget'
	r = requests.get('https://ca.search.yahoo.com/sugg/gossip/gossip-ca-ura/', params={
		'output': 'sd1',
		'command': query,
	})
	if r.status_code != 200:
		return 'command_yahoo got status code: ' + str(r.status_code)
	results = r.json()['r']
	if len(results) == 0:
		return 'no results from Yahoo! autocomplete for query: {}\ntry /google'.format(query)
	return 'Yahoo! autocomplete results for: {}\n{}'.format(query, '\n'.join(item['k'] for item in results))

def command_zel(bot, update, text: str):
	return 'atynowy'

def command_fuck(bot, update, text: str):
	return str(1/0)

def command_chatlist(bot, update, text: str):
	a = ''
	for chat_id, chat in sorted(known_chats.items()):
		a += chat.type + ':'
		if chat.type == 'private':
			if chat.first_name:
				a += ' ' + chat.first_name
			if chat.last_name:
				a += ' ' + chat.last_name
		else:
			a += ' ' + chat.title
		if chat.username:
			a += ' (@' + chat.username + ')'
		a += '\n'
	if a == '':
		return r'none ¯\_(ツ)_/¯'
	return a

def command_seen(bot, update, text: str):
	le_username = text[1:].strip().split(' ', 1)[0]
	if le_username == 'MetricBot':
		return random.choice([
			'can u not',
			'pls',
			'k',
			'no',
		])
	m = update.message
	if le_username == m.from_user.username:
		return 'piss off, ' + random.choice(faggot)
	if le_username in seen_dates:
		seconds = m.date - seen_dates[le_username]
		return 'I saw {} leik {} ago k'.format(text, seconds)
	s = 'who the fuck is {}'.format(text)
	if le_username.lower().endswith('bot'):
		s += ' (note: bots are invisible to me)'
	return s

# TODO: interface with Django site
GAMES = {
	'cybersphere': 'Cybersphere',
	'pizza_worm': 'Pizza Worm',
}
def command_game(bot, update, text: str):
	game = text.strip().lower().replace(' ', '_')
	if game == '':
		return 'games:\n' + '\n'.join(GAMES.values())
	if game not in GAMES:
		return 'invalid game name'
	m = update.message
	bot.send_game(m.chat_id, game, reply_to_message_id=m.message_id)

def command_dump(bot, update, text: str):
	m = update.message
	if m.from_user.id not in ADMINS:
		return 'fuck off'
	m2 = m.reply_to_message
	chat_id, text = text.split(' ', 1)
	if text == '':
		text = None
	if (m2 is not None) and (m2.photo is not None):
		bot.send_photo(chat_id=chat_id, photo=largest_photo(m2.photo)['file_id'], caption=text)
	else:
		return 'no photo :['

def largest_photo(photos):
	# TODO: does max width imply biggest?
	return sorted(photos, key=lambda v: v.width)[-1]

FILE_CACHE_DIR = 'file_cache'
def get_file(bot, file_id):
	makedirs(FILE_CACHE_DIR)
	path = FILE_CACHE_DIR + '/' + file_id
	if not os.path.isfile(path):
		# TODO: check if file is too big
		bot.get_file(file_id).download(path)
	return path

def get_image_id(m):
	if m.photo:
		photo = largest_photo(m.photo)
		return photo.file_id, (photo.width, photo.height), False
	if m.sticker:
		return m.sticker.file_id, (m.sticker.width, m.sticker.height), False
	if m.document and m.document.file_name.lower().endswith(('.png', '.jpg', '.jpeg')):
		# TODO: check file size
		return m.document.file_id, None, True
	return None, False

FILTERS = {
	'fftcolors': [
		'convert',
		'{in_path}',
		'-fft',
		'-delete', '1',
		'(', '-clone', '0', '+level', '50%', ')',
		'-ift',
		'{out_path}',
	],
	'fftlossy': [
		'convert',
		'{in_path}',
		'-fft',
		'-depth', '8',
		'-ift',
		'{out_path}',
	],
	'flipx': [
		'convert',
		'{in_path}',
		'-flop',
		'{out_path}',
	],
	'flipy': [
		'convert',
		'{in_path}',
		'-flip',
		'{out_path}',
	],
	'invert': [
		'convert',
		'{in_path}',
		'-negate',
		'-modulate', '100,100,0',
		'{out_path}',
	],
	'inverthue': [
		'convert',
		'{in_path}',
		'-modulate', '100,100,0',
		'{out_path}',
	],
	'jpeg': [
		'convert',
		'{in_path}',
		'-quality', '1',
		'{out_path}'
	],
	'level': [
		'convert',
		'{in_path}',
		'-auto-level',
		'{out_path}',
	],
	'negate': [
		'convert',
		'{in_path}',
		'-negate',
		'{out_path}',
	],
	'neutralize': [
		'./bin/neutralize',
		'{in_path}',
		'{out_path}',
	],
	'warp': [
		'convert',
		'{in_path}',
		'-resize', '400%',
		'-swirl', '[random:1,359]',
		'-implode', '[random*2]',
		'-resize', '25%',
		'{out_path}',
	],
}
FILTER_HELP = {
	'invert': 'Inverts the brightnesses (not the pixels)',
	'inverthue': 'Inverts the colors (not the pixels). This rotates the hue by half of a turn (180°).',
	'negate': 'Inverts the pixels',
}
def command_filter(bot, update, text: str):
	text_parts = [a for a in text.split(' ') if a != '']
	if not text_parts:
		return 'filters:\n' + '\n'.join(FILTERS.keys())

	filter_name = text_parts[0]
	if filter_name not in FILTERS:
		return 'unknown filter name'

	mr = update.message.reply_to_message
	if mr is None:
		if filter_name in FILTER_HELP:
			return FILTER_HELP[filter_name]
		return 'what'
	file_id, image_size, lossless = get_image_id(mr)
	if file_id is None:
		return 'no image :['
	m = update.message

	text_parts = text_parts[1:]
	# TODO: should I check len(m.photo)?
	bot.send_chat_action(m.chat_id, 'upload_photo')
	in_path = get_file(bot, file_id)
	out_path = make_temp_filename()
	if filter_name == 'jpeg':
		out_path += '.jpg'
	else:
		out_path += '.png'
	format_dict = {
		'in_path': in_path,
		'out_path': out_path,
	}
	f = [a.format(**format_dict) for a in FILTERS[filter_name]]
	uses_input = False
	for i in range(len(f)):
		if f[i] == '[random]':
			f[i] = str(random.random())
		elif f[i] == '[random*2]':
			f[i] = str(random.random() * 2)
		elif f[i].startswith('[random'):
			nums = [int(x) for x in f[i][8:-1].split(',')]
			f[i] = str(random.randint(nums[0], nums[1]))
		elif f[i].startswith('[input'):
			if len(text_parts) > 0:
				f[i] = ' '.join(text_parts)
			else:
				f[i] = f[i][6:-1]
			uses_input = True
	if not uses_input:
		f += text_parts
	outpoot = subprocess.run(f, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT).stdout
	if not os.path.isfile(out_path):
		if outpoot is None:
			log_message(bot, bot.send_message(chat_id=m.chat_id, text='output file is missing\ncommand: {}'.format(f)))
		else:
			log_message(bot, bot.send_message(chat_id=m.chat_id, text='output file is missing; program says: {}\ncommand: {}'.format(outpoot, f)))
	else:
		send_image(bot, m, out_path, lossless)

def command_fileid_photo(bot, update, text: str):
	if text == '':
		return 'gib id pls'
	m = update.message
	try:
		log_message(bot, bot.send_photo(chat_id=m.chat_id, photo=text, reply_to_message_id=m.message_id))
	except telegram.error.BadRequest as e:
		return str(e)

def command_fileid_document(bot, update, text: str):
	if text == '':
		return 'gib id pls'
	m = update.message
	try:
		log_message(bot, bot.send_document(chat_id=m.chat_id, document=text, reply_to_message_id=m.message_id))
	except telegram.error.BadRequest as e:
		return str(e)

def command_fileid_sticker(bot, update, text: str):
	if text == '':
		return 'gib id pls'
	m = update.message
	try:
		log_message(bot, bot.send_sticker(chat_id=m.chat_id, sticker=text, reply_to_message_id=m.message_id))
	except telegram.error.BadRequest as e:
		return str(e)

def command_fileid_video(bot, update, text: str):
	if text == '':
		return 'gib id pls'
	m = update.message
	try:
		log_message(bot, bot.send_video(chat_id=m.chat_id, video=text, reply_to_message_id=m.message_id))
	except telegram.error.BadRequest as e:
		return str(e)

def command_dump_voiceids(bot, update, text: str):
	if text == '':
		return 'gib list pls (one per line)'
	m = update.message
	ids = [x.strip() for x in text.split('\n')]
	n = 0
	for id_ in ids:
		try:
			log_message(bot, bot.send_voice(chat_id=m.chat_id, voice=id_, caption=id_))
			n += 1
		except telegram.error.BadRequest as e:
			log_message(bot, bot.send_message(chat_id=m.chat_id, text='id {} is invalid: {}'.format(id_, str(e))))
	messages = 'messages'
	if n == 1:
		messages = 'message'
	log_message(bot, bot.send_message(chat_id=m.chat_id, text='sent {} {}'.format(n, messages), reply_to_message_id=m.message_id))

CHAT_RULES = {
	CID_SHITLORDS: read_file('chat_rules/shitlords'),
}
def command_rules(bot, update, text: str):
	m = update.message
	if m.chat_id not in CHAT_RULES:
		return 'No rules defined for this group'
	return CHAT_RULES[m.chat_id]

def command_rot13(bot, update, text: str):
	return codecs.encode(text, 'rot-13')

def command_source(bot, update, text: str):
	return 'https://gitlab.com/MrMetric/MetricBot'

EMOJIFY = {
	'a': ['🅰',		],
	'b': ['🅱',		],
	'c': ['©',		'☪',	],
	'd': ['↩',		],
	'e': ['📧',		],
	'f': ['🎏',		],
	#'g': ['',		],
	'h': ['♓',		],
	'i': ['ℹ',		'🚦'],
	'j': ['🗾',		],
	'k': ['🎋',		],
	'l': ['👢',		],
	'm': ['Ⓜ',		'♏',	],
	'n': ['♑',		],
	'o': ['⭕',		],
	'p': ['🅿',		],
	#'q': ['',		],
	'r': ['🇷',		],
	's': ['⚡',		],
	't': ['🌴',		'🍸',	'⬆',	'☦'],
	'u': ['⛎',		],
	'v': ['♈',		'✅'],
	#'w': ['',		],
	'x': ['❌',		],
	'y': ['✌🏻',	],
	'z': ['Ⓩ',		],
	'!': ['❗❕',		],
	'?': ['❓',		'❔'],
}
def command_emojify(bot, update, text: str):
	tumor = ''
	for c in text:
		if c.lower() in EMOJIFY:
			tumor += random.choice(EMOJIFY[c.lower()])
		else:
			tumor += c
	return tumor

def fuck_svg(bot, m, chance, start, svg, size):
	def maybe_fuck(c):
		if c not in '0123456789':
			return c
		if random.random() < chance:
			c2 = ord(c) - ord('0')
			if random.random() < 0.5:
				c2 -= 1
			else:
				c2 += 1
			if c2 == -1:
				c2 = 9
			elif c2 == 10:
				c2 = 0
			return chr(c2 + ord('0'))
		return c
	new_svg = svg[:start] + ''.join([maybe_fuck(c) for c in svg[start:]])
	out_path = make_temp_filename()
	out_svg = out_path + '.svg'
	out_png = out_path + '.png'
	with open(out_svg, 'w') as f:
		f.write(new_svg)
	cmd = [
		'convert',
		'-size', size,
		out_svg,
		out_png,
	]
	outpoot = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout
	if not os.path.isfile(out_png):
		return outpoot.decode()
	log_message(bot, bot.send_photo(chat_id=m.chat_id, photo=open(out_png, 'rb'), reply_to_message_id=m.message_id))

THINKING_SVG = read_file('thinking.svg')
def command_thinking(bot, update, text: str):
	chance = 0.1
	if text != '':
		try:
			chance = float(text)
		except ValueError:
			return "that ain't a number"
	return fuck_svg(bot, update.message, chance, 1055, THINKING_SVG, '450x450')

TWIBELT_SVG = read_file('spike_joke_y_by_liamwhite1-d6x8bpt.svg')
def command_twifuck(bot, update, text: str):
	chance = 0.1
	if text != '':
		try:
			chance = float(text)
		except ValueError:
			return "that ain't a number"
	return fuck_svg(bot, update.message, chance, 625, TWIBELT_SVG, '1000x1000')

def command_hear(bot, update, text: str):
	if update.message.reply_to_message is None:
		return 'what'
	file_id, image_size, lossless = get_image_id(update.message.reply_to_message)
	if file_id is None:
		return 'no image :['
	in_path = get_file(bot, file_id)
	if image_size is None:
		image_size = subprocess.run([
			'convert',
			in_path,
			'-ping', '-format', '%wx%h', 'info:',
		], stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout.decode()
		image_size = image_size.split('x')
	out_path = make_temp_filename()
	out_raw = out_path + '.raw'
	out_ogg = out_path + '.ogg'
	outpoot1 = subprocess.run(['convert', in_path, 'RGB:' + out_raw], stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout
	if not os.path.isfile(out_raw):
		return outpoot1.decode()
	fmt = 's16le'
	if text == 'u':
		fmt = 'u16le'
	outpoot2 = subprocess.run([
		'ffmpeg',
		'-hide_banner',
		'-f', fmt,
		'-ar', '44100',
		'-ac', '1',
		'-i', out_raw,
		'-acodec', 'libopus',
		'-vn',
		out_ogg,
	], stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout
	if not os.path.isfile(out_ogg):
		return outpoot2.decode()
	m = update.message
	with open(out_ogg, 'rb') as f:
		caption = None
		if image_size[0] != image_size[1]:
			caption = '{}x{}'.format(*image_size)
		log_message(bot, bot.send_voice(m.chat_id, f, caption=caption, reply_to_message_id=m.message_id))

RE_IMAGE_SIZE = re.compile(r'^\d+x\d+$')
def command_see(bot, update, text: str, lossless = False, gay = False):
	mr = update.message.reply_to_message
	if mr is None:
		return 'what'
	if (not mr.voice) and (not mr.audio):
		return 'no audio :['
	if mr.voice:
		in_path = get_file(bot, mr.voice.file_id)
	else:
		in_path = get_file(bot, mr.audio.file_id)
	out_path = make_temp_filename()
	out_raw = out_path + '.raw'
	out_png = out_path + '.png'
	outpoot1 = subprocess.run([
		'ffmpeg',
		'-hide_banner',
		'-i', in_path,
		'-f', 's16le',
		'-ar', '44100',
		'-ac', '1',
		'-acodec', 'pcm_s16le',
		out_raw,
	], stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout
	if not os.path.isfile(out_raw):
		return outpoot1.decode()
	text = text.strip().lower()
	if RE_IMAGE_SIZE.match(text):
		size = text
		width, height = size.split('x')
		width, height = int(width), int(height)
		rgbsize = width * height * 3
	elif mr.caption and RE_IMAGE_SIZE.match(mr.caption):
		size = mr.caption
		width, height = size.split('x')
		width, height = int(width), int(height)
		rgbsize = width * height * 3
	else:
		dim = int((os.path.getsize(out_raw) / 3)**0.5)
		size = '{0}x{0}'.format(dim)
		rgbsize = dim * dim * 3
	if not gay:
		diff = os.path.getsize(out_raw) - rgbsize
		if diff > 0:
			size += '+' + str(diff)
	outpoot2 = subprocess.run([
		'convert',
		'-size', size,
		'-depth', '8',
		'RGB:' + out_raw,
		'-reverse', '-flatten',
		out_png,
	], stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout
	if not os.path.isfile(out_png):
		return outpoot2.decode()
	if os.path.getsize(out_png) < 1000000: # 1 MB
		subprocess.run([
			'optipng',
			'-o1',
			'-strip', 'all',
			out_png,
		])
	else:
		subprocess.run([
			'optipng',
			'-o0',
			'-strip', 'all',
			out_png,
		])
	send_image(bot, update.message, out_png, lossless)

def command_seepng(bot, update, text: str):
	return command_see(bot, update, text, True)

def command_seegay(bot, update, text: str):
	return command_see(bot, update, text, False, True)

def command_seegaypng(bot, update, text: str):
	return command_see(bot, update, text, True, True)

def command_loud(bot, update, text: str):
	mr = update.message.reply_to_message
	if mr is None:
		return 'what'
	if not (mr.voice) and (not mr.audio):
		return 'no audio :['
	if mr.voice:
		in_path = get_file(bot, mr.voice.file_id)
	else:
		in_path = get_file(bot, mr.audio.file_id)
	out_path = make_temp_filename()
	out_ogg = out_path + '.ogg'
	outpoot = subprocess.run([
		'ffmpeg',
		'-hide_banner',
		'-i', in_path,
		'-filter:a', 'loudnorm',
		'-acodec', 'libopus',
		'-vn',
		out_ogg,
	], stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout
	if not os.path.isfile(out_ogg) or os.path.getsize(out_ogg) == 0:
		return outpoot.decode()
	m = update.message
	with open(out_ogg, 'rb') as f:
		log_message(bot, bot.send_voice(m.chat_id, f, reply_to_message_id=m.message_id))

BLACKNAME1 = ['La', 'Da', 'Dar', 'Ty', 'Quen', 'Jer', 'Quin', 'Nel', 'Jam', 'Per']
BLACKNAME2 = ['shar', 'mar', 'tar', 'mal', 'tal', 'i', 'quo', 'fer', 's', 'il']
BLACKNAME3 = ['icious', 'terious', 'us', 'shon', 'ius', 'kwon', 'ondo', 'quel', 'eo', 'miah']
BLACKNAME4 = ['Jones', 'Smith', 'Williams', 'Robinson', 'Harris', 'Washington', 'Jackson', 'Carter', 'Brown', 'Johnson']
def command_blackname(bot, update, text: str):
	roll = str(random.randint(0, 99999999))
	digits = roll.zfill(4)[-4:]
	return 'roll: ' + roll + '\n' + BLACKNAME1[int(digits[0])] + BLACKNAME2[int(digits[1])] + BLACKNAME3[int(digits[2])] + ' ' + BLACKNAME4[int(digits[3])]

NOTES = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
def command_music(bot, update, text: str):
	song = []
	for i in range(random.randint(4, 16)):
		note = random.choice(NOTES)
		octave = random.randint(1, 7)
		note += str(octave)
		if random.randint(0, 9) == 0:
			note += '*'
		duration = random.randint(1, 16)
		song.append((note, duration))

	out_path = make_temp_filename()
	out_wav = out_path + '.wav'
	pysynth_b.make_wav(song, fn=out_wav, leg_stac=.7, bpm=140)
	out_ogg = out_path + '.ogg'
	outpoot = subprocess.run([
		'ffmpeg',
		'-hide_banner',
		'-i', out_wav,
		'-acodec', 'libopus',
		'-vn',
		out_ogg,
	], stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout
	if not os.path.isfile(out_ogg):
		return outpoot.decode()
	m = update.message
	with open(out_ogg, 'rb') as f:
		log_message(bot, bot.send_voice(m.chat_id, f, caption=str(song), reply_to_message_id=m.message_id))

def command_reverse(bot, update, text: str):
	mr = update.message.reply_to_message
	if mr is None:
		return 'what'

	#if (not mr.video) and (not mr.audio) and (not mr.voice) and (not mr.document) or (mr.document and not mr.document.file_name.lower().endswith('.mp4')):
	# some things hav no name
	if (not mr.video) and (not mr.audio) and (not mr.voice) and (not mr.document):
		return 'no video or audio :['

	if mr.video:
		in_path = get_file(bot, mr.video.file_id)
		ext = 'mp4'
	elif mr.audio:
		in_path = get_file(bot, mr.audio.file_id)
		ext = 'ogg'
	elif mr.voice:
		in_path = get_file(bot, mr.voice.file_id)
		ext = 'ogg'
	else:
		in_path = get_file(bot, mr.document.file_id)
		ext = 'mp4'

	out_path = make_temp_filename()
	out_file = out_path + '.' + ext
	if ext == 'mp4':
		flags = [
			'-vf', 'reverse',
			'-af', 'areverse',
			'-vcodec', 'libx264',
			'-crf', '22',
			'-profile:v', 'baseline',
			'-movflags', '+faststart',
			'-pix_fmt', 'yuv420p',
			out_file,
		]
	else:
		flags = [
			'-vn',
			'-af', 'areverse',
		]
	outpoot = subprocess.run([
		'ffmpeg',
		'-hide_banner',
		'-i', in_path,
	] + flags, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout
	if not os.path.isfile(out_file) or os.path.getsize(out_file) == 0:
		return outpoot.decode()
	m = update.message
	with open(out_file, 'rb') as f:
		if mr.video:
			log_message(bot, bot.send_video(m.chat.id, f, reply_to_message_id=m.message_id))
		elif mr.audio:
			log_message(bot, bot.send_audio(m.chat.id, f, reply_to_message_id=m.message_id))
		elif mr.voice:
			log_message(bot, bot.send_voice(m.chat.id, f, reply_to_message_id=m.message_id))
		else:
			log_message(bot, bot.send_document(m.chat.id, f, reply_to_message_id=m.message_id))

def command_loop(bot, update, text: str):
	mr = update.message.reply_to_message
	if mr is None:
		return 'what'
	if (not mr.video) and (not mr.document) or (mr.document and not mr.document.file_name.endswith('.mp4')):
		return 'no video :['
	text = text.strip()
	if text == '':
		return 'I need a number, ' + random.choice(faggot)
	try:
		n = int(text)
	except ValueError:
		return "that ain't a base ten number, " + random.choice(faggot)
	# TODO: limit by length instead of repeat count
	if n < 2 or n > 16:
		return 'number must be in range [2, 16]'
	if mr.video:
		in_path = get_file(bot, mr.video.file_id)
	else:
		in_path = get_file(bot, mr.document.file_id)
	out_path = make_temp_filename()
	out_txt = out_path + '.txt'
	with open(out_txt, 'w') as txt:
		txt.write("file '../{}'\n".format(in_path) * n)
	out_mp4 = out_path + '.mp4'
	outpoot = subprocess.run([
		'ffmpeg',
		'-hide_banner',
		'-f', 'concat',
		'-safe', '0', # it dislikes the ../
		'-i', out_txt,
		'-c', 'copy',
		out_mp4,
	], stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout
	if not os.path.isfile(out_mp4) or os.path.getsize(out_mp4) == 0:
		return outpoot.decode()
	m = update.message
	with open(out_mp4, 'rb') as f:
		log_message(bot, bot.send_video(m.chat.id, f, reply_to_message_id=m.message_id))

def replace_emoji(text: str) -> str:
	text2 = ''
	for c in text:
		if c in emoji.UNICODE_EMOJI:
			text2 += unicodedata.name(c)
			text2 += ' '
		else:
			text2 += c
	return text2

def sapi4(bot, update, text: str, voice: str, pitch: int, speed: int):
	text = text.strip()
	if text == '':
		return 'fuck off'

	text = replace_emoji(text)

	m = update.message
	bot.send_chat_action(m.chat_id, 'record_audio')

	filename = make_temp_filename()
	wavname = filename + '.wav'
	env = {
		'DISPLAY': ':0',
	}
	outpoot = subprocess.run([
		'wine',
		'bin/sapi4.exe',
		voice,
		str(pitch),
		str(speed),
		text,
		wavname,
	], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=env).stdout
	if not os.path.isfile(wavname):
		if outpoot is None:
			log_message(bot, bot.send_message(chat_id=m.chat_id, text='output file is missing', reply_to_message_id=m.message_id))
		else:
			outpoot = '\n'.join([line for line in str(outpoot, 'utf-8').splitlines() if 'fixme' not in line])
			log_message(bot, bot.send_message(chat_id=m.chat_id, text=outpoot, reply_to_message_id=m.message_id))
		return
	if os.path.getsize(wavname) == 0:
		if outpoot is None:
			log_message(bot, bot.send_message(chat_id=m.chat_id, text='output file is empty', reply_to_message_id=m.message_id))
		else:
			outpoot = '\n'.join([line for line in str(outpoot, 'utf-8').splitlines() if 'fixme' not in line])
			log_message(bot, bot.send_message(chat_id=m.chat_id, text='output file is empty; program says:\n{}'.format(outpoot), reply_to_message_id=m.message_id))
		return

	oggname = filename + '.ogg'
	outpoot = subprocess.run([
		'ffmpeg',
		'-hide_banner',
		'-i', wavname,
		'-acodec', 'libopus',
		'-b:a', '128000',
		'-vbr', 'on',
		'-compression_level', '10',
		oggname,
	], stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout
	os.remove(wavname)
	if not os.path.isfile(oggname):
		if outpoot is None:
			log_message(bot, bot.send_message(chat_id=m.chat_id, text='output file is missing ({})'.format(oggname), reply_to_message_id=m.message_id))
		else:
			outpoot = str(outpoot, 'utf-8')
			log_message(bot, bot.send_message(chat_id=m.chat_id, text='output file is missing; program says:\n{}'.format(outpoot), reply_to_message_id=m.message_id))
		return

	with open(oggname, 'rb') as f:
		log_message(bot, bot.send_voice(m.chat_id, f, reply_to_message_id=m.message_id))

def command_sam(bot, update, text: str):
	return sapi4(bot, update, text, 'Sam', 100, 150)

def command_spoderman(bot, update, text: str):
	return sapi4(bot, update, text, 'Adult Male #2, American English (TruVoice)', 400, 100)

def getint(s: str):
	try:
		return int(s, 10)
	except ValueError:
		return None

VOICE_MAP = {
	'm1': 'Adult Male #1, American English (TruVoice)',
	'm2': 'Adult Male #2, American English (TruVoice)',
	'm3': 'Adult Male #3, American English (TruVoice)',
	'm4': 'Adult Male #4, American English (TruVoice)',
	'm5': 'Adult Male #5, American English (TruVoice)',
	'm6': 'Adult Male #6, American English (TruVoice)',
	'm7': 'Adult Male #7, American English (TruVoice)',
	'm8': 'Adult Male #8, American English (TruVoice)',
	'f1': 'Adult Female #1, American English (TruVoice)',
	'f2': 'Adult Female #2, American English (TruVoice)',
	'r1': 'RoboSoft One',
	'r2': 'RoboSoft Two',
	'r3': 'RoboSoft Three',
	'r4': 'RoboSoft Four',
	'r5': 'RoboSoft Five',
	'r6': 'RoboSoft Six',
	'spoderman': 'Adult Male #2, American English (TruVoice)',

	'adult male #1, american english (truvoice)': 'Adult Male #1, American English (TruVoice)',
	'adult male #2, american english (truvoice)': 'Adult Male #2, American English (TruVoice)',
	'adult male #3, american english (truvoice)': 'Adult Male #3, American English (TruVoice)',
	'adult male #4, american english (truvoice)': 'Adult Male #4, American English (TruVoice)',
	'adult male #5, american english (truvoice)': 'Adult Male #5, American English (TruVoice)',
	'adult male #6, american english (truvoice)': 'Adult Male #6, American English (TruVoice)',
	'adult male #7, american english (truvoice)': 'Adult Male #7, American English (TruVoice)',
	'adult male #8, american english (truvoice)': 'Adult Male #8, American English (TruVoice)',
	'adult female #1, american english (truvoice)': 'Adult Female #1, American English (TruVoice)',
	'adult female #2, american english (truvoice)': 'Adult Female #2, American English (TruVoice)',
	'female whisper': 'Female Whisper',
	'male whisper': 'Male Whisper',
	'mary': 'Mary',
	'mary (for telephone)': 'Mary (for Telephone)',
	'mary in hall': 'Mary in Hall',
	'mary in space': 'Mary in Space',
	'mary in stadium': 'Mary in Stadium',
	'mike': 'Mike',
	'mike (for telephone)': 'Mike (for Telephone)',
	'mike in hall': 'Mike in Hall',
	'mike in space': 'Mike in Space',
	'mike in stadium': 'Mike in Stadium',
	'robosoft one': 'RoboSoft One',
	'robosoft two': 'RoboSoft Two',
	'robosoft three': 'RoboSoft Three',
	'robosoft four': 'RoboSoft Four',
	'robosoft five': 'RoboSoft Five',
	'robosoft six': 'RoboSoft Six',
	'sam': 'Sam',
}
def command_tts(bot, update, text: str):
	'''
	TTS using Microsoft's SAPI (version 4)
	Syntax: /tts voice;pitch;speed;text
	Example: /tts Sam;100;400;soi soi soi soi soi soi
	Voice list:
		f1 = Adult Female #1, American English (TruVoice)
		f2 = Adult Female #2, American English (TruVoice)
		m1 = Adult Male #1, American English (TruVoice)
		...
		m8 = Adult Male #8, American English (TruVoice)
		Female Whisper
		Male Whisper
		Mary
		Mary (for Telephone)
		Mary in Hall
		Mary in Space
		Mary in Stadium
		Mike
		Mike (for Telephone)
		Mike in Hall
		Mike in Space
		Mike in Stadium
		RoboSoft One
		RoboSoft Two
		RoboSoft Three
		RoboSoft Four
		RoboSoft Five
		RoboSoft Six
		Sam
	'''
	parts = text.strip().split(';')
	if len(parts) < 4:
		return 'syntax: voice;pitch;speed;text'
	voice = parts[0].strip().lower()
	if voice in VOICE_MAP:
		voice = VOICE_MAP[voice]
	pitch = getint(parts[1].strip())
	if pitch is None:
		return 'syntax: voice;pitch (an integer);speed;text'
	speed = getint(parts[2].strip())
	if speed is None:
		return 'syntax: voice;pitch;speed (an integer);text'
	text_ = ';'.join(parts[3:])
	return sapi4(bot, update, text_, voice, pitch, speed)

def command_gif(bot, update, text: str):
	m = update.message
	log_message(bot, bot.send_document(chat_id=m.chat_id, document=text, reply_to_message_id=m.message_id))

# SELECT COUNT(*) FROM "cums_-1001256555336" WHERE user_id IS 132410182 AND 

def sqlite_query_1result(dbc, query):
	dbc.execute(query)
	rows = dbc.fetchall()
	assert(len(rows) == 1)
	assert(len(rows[0]) == 1)
	return rows[0][0]

def command_cum(bot, update, text: str):
	m = update.message
	chat_id = update.message.chat.id
	user_id = m.from_user.id

	db = sqlite3.connect("MetricBot.db")
	dbc = db.cursor()

	dbc.execute(r"""
	CREATE TABLE IF NOT EXISTS "cums_{}" (
		"user_id" INTEGER NOT NULL,
		"timestamp" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
	);""".format(chat_id))

	dbc.execute(r"""INSERT INTO "cums_{}"(user_id) VALUES({});""".format(chat_id, user_id))

	g_count = sqlite_query_1result(dbc, r"""SELECT COUNT() FROM "cums_{}";""".format(chat_id))
	u_count = sqlite_query_1result(dbc, r"""SELECT COUNT() FROM "cums_{}" WHERE user_id = {};""".format(chat_id, user_id))
	g_count_1day = sqlite_query_1result(dbc, r"""SELECT COUNT() FROM "cums_{}" WHERE timestamp >= datetime('now','-1 day');""".format(chat_id))
	u_count_1day = sqlite_query_1result(dbc, r"""SELECT COUNT() FROM "cums_{}" WHERE user_id = {} AND timestamp >= datetime('now','-1 day');""".format(chat_id, user_id))

	db.commit()
	db.close()

	return "You just came! Stats: {}/{} (last 24 hours: {}/{})".format(u_count, g_count, u_count_1day, g_count_1day)

def command_cumstats(bot, update, text: str):
	m = update.message
	chat_id = update.message.chat.id
	user_id = m.from_user.id

	db = sqlite3.connect("MetricBot.db")
	dbc = db.cursor()

	g_count = sqlite_query_1result(dbc, r"""SELECT COUNT() FROM "cums_{}";""".format(chat_id))
	u_count = sqlite_query_1result(dbc, r"""SELECT COUNT() FROM "cums_{}" WHERE user_id = {};""".format(chat_id, user_id))
	g_count_1day = sqlite_query_1result(dbc, r"""SELECT COUNT() FROM "cums_{}" WHERE timestamp >= datetime('now','-1 day');""".format(chat_id))
	u_count_1day = sqlite_query_1result(dbc, r"""SELECT COUNT() FROM "cums_{}" WHERE user_id = {} AND timestamp >= datetime('now','-1 day');""".format(chat_id, user_id))

	db.close()

	return "Your cum count is {}. The group's total is {}.\nIn the last 24 hours, your count is {}, and the group's total is {}.".format(u_count, g_count, u_count_1day, g_count_1day)

commands = {}

def command_worthlessshit(bot, update, text: str):
	chat_id = update.message.chat_id
	if chat_id in COMMAND_WHITELIST:
		c = COMMAND_WHITELIST[chat_id]
	else:
		c = commands.keys()
		if chat_id in COMMAND_BLACKLIST:
			for name in COMMAND_BLACKLIST[chat_id]:
				c.remove(name)
	c = sorted(c)
	return '{} commands:\n/'.format(len(c)) + ' /'.join(c)

WORDS_14 = 'We must secure the existence of our people and a future for white children.'
WORDS_88 = 'What we must fight for is to safeguard the existence and reproduction of our race and our people, the sustenance of our children and the purity of our blood, the freedom and independence of the fatherland, so that our people may mature for the fulfillment of the mission allotted it by the creator of the universe. Every thought and every idea, every doctrine and all knowledge, must serve this purpose. And everything must be examined from this point of view and used or rejected according to its utility.'
WORDS_1488 = WORDS_14 + '\n\n' + WORDS_88

SIMPLE_RESPONSES = {
	'maybe': 'fuck off',
	'fuck off': 'maybe',
	#'kek': 'bur',
	'KEK': 'BUR',
	'k-kek': 'b-bur',
	'bur': 'kek',
	'topkek': 'bottomquark',
	'top kek': 'bottom quark',
	'lael': 'læl',
	'loel': 'lœl',
	'cyka': 'blyat',
	'cheeki': 'breeki',
	'lel': 'ləl',
	':^)': ':^D',

	'hot': 'cold',
	'cold': 'hot',
	'warm': 'cool',
	'cool': 'warm',

	'in': 'out',
	'out': 'in',
	'up': 'down',
	'down': 'up',
	'wrong': 'right',
	'black': 'white',
	'fight': 'break up',
	'kiss': 'make up',

	'suck my dick': 'suck my motherfuckin\' dick dick',

	'hue'					: 'swift sat solver',
	'ayy'					: 'lmao',
	'eh'					: 'sexy lady',
	'14'					: WORDS_14,
	'88'					: WORDS_88,
	'1488'					: WORDS_1488,
	'14 88'					: WORDS_1488,
	'bitch'					: 'female dog',
	'haha'					: '[h]a[h]a',
	'HALE'					: 'HORTLER',
	'HEIL'					: 'HITLER',
	'WAKE ME UP INSIDE'		: "(CAN'T WAKE UP)",

	# TODO: be less retarded
	'deus'					: 'vult',
	'Deus'					: 'Vult',
	'DEus'					: 'VUlt',
	'DEUs'					: 'VULt',
	'DEUS'					: 'VULT',
	'dEus'					: 'vUlt',
	'dEUs'					: 'vULt',
	'dEUS'					: 'vULT',
	'deUs'					: 'vuLt',
	'deUS'					: 'vuLT',
	'deuS'					: 'vulT',

	'noice': '*nice',
	'Noice': '*Nice',
	'NOICE': '*NICE',
	'shiet': '*shit',
	'Shiet': '*Shit',
	'SHIET': '*SHIT',

	'heh': 'hah',
	'Heh': 'Hah',
	'HEH': 'HAH',

	'this': 'that',
	'This': 'That',
	'THIS': 'THAT',

	'literally this': 'literally that',
	'Literally this': 'Literally that',
	'literally THIS': 'literally THAT',
	'Literally THIS': 'Literally THAT',
	'LITERALLY THIS': 'LITERALLY THAT',
	'literally fucking this': 'literally fucking that',
	'Literally fucking this': 'Literally fucking that',
	'literally fucking THIS': 'literally fucking THAT',
	'Literally fucking THIS': 'Literally fucking THAT',
	'LITERALLY FUCKING THIS': 'LITERALLY FUCKING THAT',

	'test': 'icles',
	'Test': 'icles',
	'TEST': 'ICLES',

	'kocham': 'ciȩ',

	#'gay': 'straight',
	#'straight': 'gay',

	'What the fuck did you just fucking say about me, you little bitch?': 'I’ll have you know I graduated top of my class in the Navy Seals, and I’ve been involved in numerous secret raids on Al-Quaeda, and I have over 300 confirmed kills. I am trained in gorilla warfare and I’m the top sniper in the entire US armed forces. You are nothing to me but just another target. I will wipe you the fuck out with precision the likes of which has never been seen before on this Earth, mark my fucking words. You think you can get away with saying that shit to me over the Internet? Think again, fucker. As we speak I am contacting my secret network of spies across the USA and your IP is being traced right now so you better prepare for the storm, maggot. The storm that wipes out the pathetic little thing you call your life. You’re fucking dead, kid. I can be anywhere, anytime, and I can kill you in over seven hundred ways, and that’s just with my bare hands. Not only am I extensively trained in unarmed combat, but I have access to the entire arsenal of the United States Marine Corps and I will use it to its full extent to wipe your miserable ass off the face of the continent, you little shit. If only you could have known what unholy retribution your little “clever” comment was about to bring down upon you, maybe you would have held your fucking tongue. But you couldn’t, you didn’t, and now you’re paying the price, you goddamn idiot. I will shit fury all over you and you will drown in it. You’re fucking dead, kiddo.',
	'What ze fuck did you just fucking say about me, you verdammter Jude?': 'I’ll have you know I graduated top of mein class in ze Hitlerjugend, and I’ve been involved in numerous secret Gestapo raids in Berlin, and I have over 300 million confirmed executions. I am trained in gorilla gassing and I am ze top sniper in ze entire Wehrmacht. You are nothing to me but just another race traitor. I will wipe you ze fuck out with precision ze likes of which has never been seen before on this Reich, mark mein fucking words. You think you can get away with saying zat scheiße to me over ze Internet? Think again, arschloch. As vee speak I am contacting mein secret network of spies across Deutschland and your IP is being traced right now so you better prepare for ze storm, blödel. Ze storm zat wipes out ze pathetic little thing you call your life. You’re fucking dead, kid. I can be anywhere, anytime, and I can kill you in over seven hundred ways, and zat’s just with mein machinenpistole. Not only am I extensively trained in unarmed kampf, but I have access to ze entire arsenal of ze Wehrmacht forces and I will use it to its full extent to wipe your miserable arsch off ze face of ze Reich, you little shit. If only you could have known what unholy blitzkrieg your little "clever" kommentar was about to bring down upon you, maybe you would have held your fucking tongue. But you couldn’t, you didn’t, and now you’re paying ze price, you verdammter dummkopf. I will shit Zyklon B all over you and you will drown in it. You’re fucking dead, kiddo.',
	'What the craft did you just say to me you little creeper?': 'I’ll have you know I graduated top of my class in the Mithrina Academy, and have been involved in numerous secret griefs with Team Avolition, and I have griefed over 300 servers. I am trained in creeper warfare and I’m the top skeleton in all of the minecraft griefing teams in existance. You are nothing to me but just another block. I will blow you up with precisiion the likes of which has never been seen before in this blocky world. Notch my fucking pickaxe. You think you can get away with griefing me like that? Think again, blockhead. As we speak I am contacting my secret network of creepers across my server and your coords traced right now so you better prepare for the storm, zombie. The storm that wipes out the pathetic little thing you call your life. You’re blocking dead, kid. I can teleport anywhere, anytime, and I can kill you in over 700 ways, and that’s just with my wooden sword. Not only am I extensively trained in PvP, but I have have access to all the chests on my server and I will use them to their full extent to ban your miserable block off the face of the server, you little block. If ony you could have known what unholy retribution your "clever" comment was about to TNT down upon you. Maybe you would have held your fucking pickaxe. But you couldn’t, you didn’t, and now you’re paying the price, you blockhead. I will drop gravel all over you, and you will suffocate in it. You’re blocking dead, blockface.',
}
SMILEY_FACES = [
	(':]' , ':['),
	(':D' , 'D:'),
	(':)' , ':('),
	(':>' , ':<'),
	('>:[', '>:]'),
	('>:D', 'D:<'),
	('=D' , 'D='),
	(':^D', 'D^:'),
	('B^U', 'U^B'),
	('c:' , ':c'),
	('C:' , ':C'),
]
SMILEY_FACE_PAIRS = [
	('ye', 'ne'),
	('yes', 'no'),
	('Yes', 'No'),
	('YES', 'NO'),
	('no u', 'yes me'),
	('yes u', 'no me'),

	('ja', 'nein'),
	('JA', 'NEIN'),
	('Ja', 'Nein'),
]
for face in SMILEY_FACES:
	for pair in SMILEY_FACE_PAIRS:
		SIMPLE_RESPONSES[pair[0] + ' ' + face[0]] = pair[1] + ' ' + face[1]
		SIMPLE_RESPONSES[pair[0] + ' ' + face[1]] = pair[1] + ' ' + face[0]
		SIMPLE_RESPONSES[pair[1] + ' ' + face[0]] = pair[0] + ' ' + face[1]
		SIMPLE_RESPONSES[pair[1] + ' ' + face[1]] = pair[0] + ' ' + face[0]

SIMPLE_RESPONSES_I = {
	'hey'									: 'I just met you',
	'and this is crazy'						: "but here's my number",
	'so call me maybe'						: '👍🏿',

	'look at this photograph'				: 'every time I do it makes me laugh',

	'party rock is in the house tonight'	: 'everybody just have a good time',
	'and we gonna make you lose your mind'	: 'everybody just have a good time',
	'and we gon make you lose your mind'	: 'everybody just have a good time',
	"and we gon' make you lose your mind"	: 'everybody just have a good time',

	'wake me up'			: 'wake me up inside',
	'wake me up inside'		: "I can't wake up",
	"i can't wake up"		: 'save me',

	'how could this happen to me'	: "I've made my mistakes",
	'how could this happen to me?'	: "I've made my mistakes",
	"i've made my mistakes"			: 'got nowhere to run',
	'got nowhere to run'			: 'the night goes on',
	'the night goes on'				: "as I'm fadin' away",
	"as i'm fadin' away"			: "I'm sick of this life",
	"i'm sick of this life"			: "I just wanna scream",
	'i just wanna scream'			: 'how could this happen to me?',

	"don't be stupid"				: 'be a smarty',
	'be a smarty'					: 'come and join the Nazi party',
	'come and join the nazi party'	: '卐卐卐卐',

	'jesus christ'					: '*mormon jesus',
	'jesus fuck'					: '*mormon fuck',
	'jesus babyfucking christ'		: '*jesus mormonfucking christ',

	'nop'							: '0x90',
}

def trigger_exact_penis(m):
	caps = [c.isupper() for c in m.text]
	def doshit(butts):
		for butt in butts:
			if butt[0]:
				yield butt[1].upper()
			else:
				yield butt[1].lower()
	sentence = ' '.join(doshit(zip(caps, ['heil', 'yea', 'gimme', 'some', 'penis'])))
	return sentence

def trigger_include_cancer(m):
	return 'Hav you tried asking @kanserbot?'

EBOLAS = [m for m in read_file('ebolas').split('\n') if m != '']
def trigger_include_ebola(m):
	return random.choice(EBOLAS)

def trigger_include_php(m):
	return 'PHP is a blight upon society. Stop being such a {}.'.format(random.choice(faggot))

def trigger_include_java(m):
	if ('cup' in m.text) or ('drink' in m.text):
		return 'drink C# instead k'
	return 'use C# instead k'

def trigger_include_rating(m):
	return m.text.replace('10/10', '9/11')

#def trigger_include_ae_oe(text):
#	return text.replace('ae', 'æ').replace('oe', 'œ')

def trigger_include_lol(m):
	words = m.text.split(' ')
	if 'LOL' in words:
		return 'LO' * random.randint(1, 8) + 'L'
	if 'Lol' in words:
		return 'Lo' + 'lo' * random.randint(0, 7) + 'l'
	if 'lol' in m.text.lower().split(' '):
		return 'lo' * random.randint(1, 8) + 'l'
	return ''

def trigger_include_lmao(m):
	return 'lm' + 'a' * random.randint(1, 8) + 'o'

def trigger_include_omg(m):
	return 'om' + 'g' * random.randint(1, 8)

def trigger_include_omfg(m):
	return 'omf' + 'g' * random.randint(1, 8)

def trigger_include_xd(m):
	return 'x' * random.randint(1, 8) + 'd' * random.randint(1, 8)

def trigger_include_woo(m):
	return 'wo' + 'o' * random.randint(1, 8)

def trigger_include_statist(m):
	return m.text.lower().replace('statist', 'statistician')
def trigger_include_stattist1(m):
	return m.text.lower().replace('st@tist', 'statistician')
def trigger_include_stattist2(m):
	return m.text.lower().replace('st@ist', 'statistician')

def trigger_include_ifunny(m):
	return random.choice([
		'iFunny sucks k',
		'stop talking aboot iFunny',
		'fuck off with that iFunny garbage',
		'iFunny = cancer',
	])

def trigger_include_kafir(m):
	return m.text.replace('kafir', 'yuika').replace('KAFIR', 'YUIKA').replace('Kafir', 'Yuika')

TRIGGERS = {
	#'cancer': trigger_include_cancer,
	'ebola': trigger_include_ebola,
	#'php': trigger_include_php,
	#'java': trigger_include_java,
	'10/10': trigger_include_rating,
	#'ae': trigger_include_ae_oe,
	#'oe': trigger_include_ae_oe,
	'lol': trigger_include_lol,
	'lmao': trigger_include_lmao,
	'omg': trigger_include_omg,
	'omfg': trigger_include_omfg,
	'xd': trigger_include_xd,
	'woo': trigger_include_woo,
	'statist': trigger_include_statist,
	'st@tist': trigger_include_stattist1,
	'st@ist': trigger_include_stattist2,
	'ifunny': trigger_include_ifunny,
	'kafir': trigger_include_kafir,
}

def start_cmdls(m):
	text = m.text[5:]
	outpoot = str(subprocess.run(['/usr/bin/ls'] + text.split(' '), stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout, 'utf-8')
	if len(outpoot) > 1024:
		outpoot = outpoot[:1024]
		outpoot += '…\n\nshit is truncated k'
	return outpoot

def start_cmdcat(m):
	text = m.text[6:]
	if ('SUPER_SEEKRET_API_KEY' in text) or ('BOORU_KEY' in text):
		return 'that is a secret you faggot'

	outpoot = subprocess.run(['/usr/bin/cat'] + text.split(' '), stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout
	try:
		outpoot = str(outpoot, 'utf-8')
	except UnicodeDecodeError:
		outpoot = 'that shit ain\'t UTF-8'
	if len(outpoot) > 1024:
		outpoot = outpoot[:1024]
		outpoot += '…\n\nshit is truncated k'
	return outpoot

def start_cmdpwd(m):
	return os.getcwd()

def start_cmdrm(m):
	text = m.text[5:]
	if text == 'kebab':
		return 'https://telegram.me/WhiteIsRight/520'
	return 'FUCK OFF'

def start_reminder(m):
	return 'Reminder that you are a faggot'

def start_note(m):
	return "I'd like to note that you are a faggot"

starts = {
	#'$ ls ': start_cmdls,
	#'$ cat ': start_cmdcat,
	'$ pwd': start_cmdpwd,
	'$ rm ': start_cmdrm,
	'Reminder that ': start_reminder,
	"I'd like to note that ": start_note,
}

#ERECT = 'big fucking erect '

def process_command_text(text):
	shit = text[1:].split(' ', 1)
	if len(shit) == 1:
		command = shit[0]
		text = ''
	else:
		command, text = shit
	command = command.lower()
	targeted = False
	if command.endswith(atname_lower):
		command = command[:-len(atname_lower)]
		targeted = True
	elif '@' in command:
		targeted = None
	return (command, text, targeted)

def do_swap(bot, update, text, word1, word2):
	if (word1 in text) or (word2 in text):
		parts = [part.strip() for part in text.split(' ') if part.strip() != '']
		excrement = [part for part in parts if (part != word1) and (part != word2)]
		if len(excrement) == 0:
			def swap(part):
				if part == word2:
					return word1
				return word2
			parts = [swap(part) for part in parts]
			send_message(bot, update.message.chat_id, ' '.join(parts))
			return True
	return False

def process_message(update, context):
	m = update.message
	try:
		process_message_(context.bot, update)
	except telegram.error.BadRequest as e:
		send_message(bot, m.chat_id, 'Telegram API error: {}'.format(e))
	except Exception:
		error = traceback.format_exc().replace(DERPIBOORU_KEY, 'DERPIBOORU_KEY').replace(PONYBOORU_KEY, 'PONYBOORU_KEY')
		send_message(bot, m.chat_id, 'Error processing message:\n\n```{}```'.format(error), parse_mode='Markdown', reply_to_message_id=m.message_id)

def process_message_(bot, update):
	m = update.message
	if m is None:
		# TODO
		return
	log_message(bot, m)
	if m.text is None:
		return

	text = m.text
	text_lower = text.lower()

	if text.startswith('/'):
		_, _, targeted = process_command_text(text)
		if targeted or update.message.chat.type == 'private':
			send_message(bot, m.chat_id, "that ain't one of my commands, brah", reply_to_message_id=m.message_id)
			return

	if m.reply_to_message != None:
		log('got reply to {}'.format(m.reply_to_message.from_user.username))

	'''
	text_lower_r = text_lower.replace('  ', ' ')
	if ERECT in text_lower_r:
		word = text[text_lower_r.find(ERECT) + len(ERECT):]
		if text_lower_r.endswith('!'):
			text_lower_r = text_lower_r[:-1]
			word = word[:-1]
		if text_lower_r.endswith(', ma'):
			word = word[:-4]
		elif text_lower_r.endswith(', mom') or text_lower_r.endswith(', mum'):
			word = word[:-5]
		send_message(bot, m.chat_id, "DON'T SAY " + word.upper() + ' IN THIS HOUSE')
		return
	'''

	targeted = False
	if text.lower().startswith(atname_lower):
		text = text[len(atname_lower):].lstrip()
		targeted = True
	'''
	else:
		# for when privacy is disabled
		return
	'''

	if targeted:
		if text == 'restart you fucking piece of shit':
			send_message(bot, m.chat_id, 'Restarting, ' + random.choice(faggot), reply_to_message_id=m.message_id)
			time.sleep(1)
			# TODO: exit self after spawning new thingy?
			os.execv(__file__, sys.argv)
			return

		if text == 'restart':
			send_message(bot, m.chat_id, random.choice([
				'mcfucking kill yourself',
				'ill restart ur mum m8',
				'fite me',
				'speak english you nigger'
			]), reply_to_message_id=m.message_id)
			return

		if text == 'shitpost pls':
			if m.chat_id in COOL_CHATS:
				send_message(bot, m.chat_id, 'I already am, ' + random.choice(faggot), reply_to_message_id=m.message_id)
			else:
				COOL_CHATS.append(m.chat_id)
				send_message(bot, m.chat_id, 'sure thing, you lazy fuck', reply_to_message_id=m.message_id)
			return

		if text == 'shut the fuck up':
			if m.chat_id in COOL_CHATS:
				COOL_CHATS.remove(m.chat_id)
				send_message(bot, m.chat_id, 'whatever, faggot', reply_to_message_id=m.message_id)
			else:
				send_message(bot, m.chat_id, 'what the fuck do you expect from me', reply_to_message_id=m.message_id)
			return

		if text == 'shitpost harder':
			if m.chat_id in SHITPOST_HARDER:
				send_message(bot, m.chat_id, 'already on it, ' + random.choice(faggot), reply_to_message_id=m.message_id)
			else:
				SHITPOST_HARDER.append(m.chat_id)
				send_message(bot, m.chat_id, 'that makes me moist', reply_to_message_id=m.message_id)
			return

		if text == 'shitpost softer':
			if m.chat_id in SHITPOST_HARDER:
				SHITPOST_HARDER.remove(m.chat_id)
				send_message(bot, m.chat_id, 'boner killed', reply_to_message_id=m.message_id)
			else:
				send_message(bot, m.chat_id, "but i don't have a boner", reply_to_message_id=m.message_id)
			return

	if text == '$ ls':
		text = '$ ls .'
	for start, func in starts.items():
		if text.startswith(start):
			send_message(bot, m.chat_id, func(m), reply_to_message_id=m.message_id)
			return

	if m.chat_id in COOL_CHATS:
		for trigger, func in TRIGGERS.items():
			if trigger in text_lower:
				send_message(bot, m.chat_id, func(m), reply_to_message_id=m.message_id)
				return

		#if 'c#' in text_lower:
		#	send_message(bot, m.chat_id, "Microsoft's Musical Language: makes code sing!", reply_to_message_id=m.message_id)
		#	return

		if do_swap(bot, update, text, 'yes', 'no'):
			return
		if do_swap(bot, update, text, 'ye', 'ne'):
			return
		if do_swap(bot, update, text, 'ja', 'nein'):
			return

		if text in SIMPLE_RESPONSES:
			send_message(bot, m.chat_id, SIMPLE_RESPONSES[text], reply_to_message_id=m.message_id)
			return
		if text_lower in SIMPLE_RESPONSES_I:
			send_message(bot, m.chat_id, SIMPLE_RESPONSES_I[text_lower], reply_to_message_id=m.message_id)
			return
		if m.sticker and m.sticker.file_id == 'CAADAQADqiMAAnj8xgUHCgS5Cko10QI':
			send_message(bot, m.chat_id, 'MAYBE', reply_to_message_id=m.message_id)
			return

	if text_lower == 'penis':
		send_message(bot, m.chat_id, trigger_exact_penis(m), reply_to_message_id=m.message_id)
		return
	if len(text_lower) == 6 and text_lower.startswith('penis'):
		send_message(bot, m.chat_id, trigger_exact_penis(m) + text_lower[5], reply_to_message_id=m.message_id)
		return

	if 'pepe' in text:
		send_message(bot, m.chat_id, text.replace('pepe', 'pay pay'), reply_to_message_id=m.message_id)
		return

	if text == ':^)':
		send_message(bot, m.chat_id, ' ̈͜>')

	if 'needs more jpeg' in text_lower:
		command_filter(bot, update, 'jpeg')
		return
	if 'needs more fft' in text_lower:
		command_filter(bot, update, 'fftlossy')
		return

	if text_lower.startswith('sam, say '):
		command_sam(bot, update, text[9:])
		return

	if targeted or m.chat_id in SHITPOST_HARDER:
		send_message(bot, m.chat_id, commands['congrats'](bot, update, text), reply_to_message_id=m.message_id)

def handle_inline(update, context):
	query = update.inline_query
	if not query:
		return
	log('{} {} (@{}): @{} {}'.format(query.from_user.first_name, query.from_user.last_name, query.from_user.username, context.bot.username, query.query))

	text = query.query
	if text.startswith('/'):
		command, text, targeted = process_command_text(text)
		if command in commands:
			shit = commands[command](context.bot, update, text)
		else:
			shit = "that ain't one of my commands, brah"
	else:
		shit = 'type a command, dillbag'
	if shit:
		results = []
		if type(shit) == Shitstain:
			if shit.type == ShitstainType.text:
				results.append(
					InlineQueryResultArticle(
						id=query.id + '_result0',
						title=shit.data,
						input_message_content=InputTextMessageContent(shit.data)
					)
				)
			elif shit.type == ShitstainType.voice:
				results.append(
					InlineQueryResultVoice(
						id=query.id + '_result0',
						voice_url=None, # what the fuck
						title=shit.content.title,
						caption=shit.content.caption,
					)
				)
			elif shit.type == ShitstainType.photo:
				# why the hell do I need to use URLs instead of file IDs
				pass
		else:
			results.append(
				InlineQueryResultArticle(
					id=query.id + '_result0',
					title=shit,
					input_message_content=InputTextMessageContent(shit)
				)
			)
		context.bot.answer_inline_query(query.id, results, cache_time=0) # TODO: set cache_time as necessary

def rotate(x, n: int, width=8):
	if width < 1:
		raise ValueError()
	n = n % width
	if n == 0:
		return x

	all_bits = 2**width - 1
	a = ((x & all_bits) >> n)
	b = (x << (width - n) & all_bits)
	return a | b

KEY = b'A ranarep yo lovep toleratet Twilight Sparkle'
KEY_LEN = len(KEY)

def encrypt(s: bytes) -> bytes:
	e = b''
	for i, x in enumerate(s):
		y = rotate(x, 3) ^ KEY[i % KEY_LEN]
		e += bytes([y])
	return e

def handle_callback_query(update, context):
	q = update.callback_query
	game = q.game_short_name
	if game is None:
		q.answer(text='???')
		return

	vals = {
		'u': q.from_user.id,
	}

	if q.message is None:
		vals['i'] = q.inline_message_id
	else:
		vals['c'] = q.message.chat_id
		vals['m'] = q.message.message_id

	vals = json.dumps(vals, separators=(',', ':')).encode('UTF-8')
	params = {
		'id': base64.urlsafe_b64encode(encrypt(vals)),
		'name': q.from_user.first_name,
	}
	if q.from_user.last_name:
		params['name'] += ' ' + q.from_user.last_name

	r = requests.models.PreparedRequest()
	r.prepare_url('https://thingies.site/game/' + game, params)
	q.answer(url=r.url)

COMMAND_PREFIX = 'command_'
RANDOM_CHOICE_DIR = 'random_choice'
def find_commands():
	for name in globals():
		if name.startswith(COMMAND_PREFIX):
			command_name = name[len(COMMAND_PREFIX):]
			commands[command_name] = globals()[name]
			log('added command: {}'.format(command_name), False)

	commands[':^)'] = lambda bot, update, text: ' ̈͜>'

	random_choice = os.listdir(RANDOM_CHOICE_DIR)
	for name in random_choice:
		the_list = read_file(RANDOM_CHOICE_DIR + '/' + name).split('\n\n')
		the_list = [m.replace('\\n', '\n').strip() for m in the_list]
		the_list = [m for m in the_list if m != '']
		# le_list because pylint is confused by the_list=the_list
		commands[name] = lambda bot, update, text, le_list=the_list: random.choice(le_list)
		log('added random.choice command \'{}\' with {} entries'.format(name, len(the_list)), False)

def make_handler(func):
	def handler(update, context):
		log_message(context.bot, update.message)

		m = update.message
		command, text, targeted = process_command_text(update.message.text)
		if targeted is None:
			return

		if m.chat_id in COMMAND_WHITELIST and command not in COMMAND_WHITELIST[m.chat_id]:
			text = "that command is disabled here"
			send_message(context.bot, m.chat_id, text, reply_to_message_id=m.message_id)
			return

		if m.chat_id in COMMAND_BLACKLIST and command in COMMAND_BLACKLIST[m.chat_id]:
			text = "that command is disabled here"
			send_message(context.bot, m.chat_id, text, reply_to_message_id=m.message_id)
			return

		try:
			text = func(context.bot, update, text)
		except CommandError as e:
			text = str(e)
		except telegram.error.BadRequest as e:
			text = 'Telegram API error: {}'.format(e)
		except Exception:
			error = traceback.format_exc()
			msg = 'Error processing message:\n\n```Traceback\n{}```'.format(error)
			send_message(context.bot, m.chat_id, msg, parse_mode='Markdown', reply_to_message_id=m.message_id)
			return

		send_message(context.bot, m.chat_id, text, reply_to_message_id=m.message_id)
	return handler

def main():
	logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

	bot = telegram.Bot(token=read_file('SUPER_SEEKRET_API_KEY'))

	if not bot.username: # connection failed?
		log('failed to start {}'.format(bot), False)
		exit(1)
	log(bot.username)
	global atname_lower
	atname_lower = '@' + bot.username.lower()

	updater = telegram.ext.Updater(bot=bot)
	find_commands()
	for name, func in commands.items():
		updater.dispatcher.add_handler(CommandHandler(name, make_handler(func)))
	updater.dispatcher.add_handler(MessageHandler(telegram.ext.Filters.all, process_message))
	updater.dispatcher.add_handler(InlineQueryHandler(handle_inline))
	updater.dispatcher.add_handler(CallbackQueryHandler(handle_callback_query))
	updater.start_polling()

if __name__ == '__main__':
	main()
